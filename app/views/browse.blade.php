@extends('layouts.default')

@section('page-title')
Browse
@stop

@section('page-content')

	@if(Session::has('success'))
		<p class="center alert alert-success">{{ Session::get('success') }}</p>
	@endif
	@if(Session::has('error'))
		<p class="center alert alert-error">{{ Session::get('error') }}</p>
	@endif

	<div id="browse" class="container container-single clearfix">
		<div class="fake-center">
		<h1>Browse</h1>
			{{ Form::open(array('url' => 'browse')) }}
			<div class="add-show">{{ Form::button('Add to My Shows', array('class'=>'button button-medium', 'type'=>'submit')) }}</div>
				@foreach($shows as $show)
				<div class="mini-container">
					<div><p title="{{ $show->title }}" class='ellipsis'><strong>{{ $show->title }}</strong></p>
						@if($watched->contains($show))
						@else
							{{ Form::checkbox('select'.$show->id.'', $show->id) }}
						@endif
					</div>
					<a href="/browse/series/{{ $show->id }}/{{ str_replace(' ','-', $show->title) }}">{{ HTML::image($covers_directory.'/'.$show->cover, $show->title) }}</a>
					<div>Rated <strong>{{ $show->cache->avg_rating }}</strong> by {{ $show->cache->total_user_ratings }} @if($show->cache->total_user_ratings == 1) user @else users @endif</div>
				</div>
				@endforeach
			{{ Form::close() }}
		</div>
	</div>

@stop