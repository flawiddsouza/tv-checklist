#0.7.0 (2015-00-00)
- Sortable tables on the profile page, how cool is that?
- Ability to login using your email address as well as your username (prior to this, the user could only login using the username)
- [Class 'tvshow' not found" error] while accessing the 'My Shows' page, has now been fixed by replacing 'tvshow' with 'TVShow' in the 'WatchedTVShow' model
- Corrected the HTML code to be valid HTML5
- Meta description and meta charset properties have been added to all the html pages
- Rich Snippets Have Been Enabled for TV Show Ratings (So that the google results show the ratings along with the tvshow's page url)
- 'Joined on' date+time now displayed in the Admin Panel's Manage Users page
- Auto-updating episode listing for ongoing tvshows

#0.6.0 (2015-02-25)
- Fully functional search bar and search results page
- Added 'User Profiles'
- Now the average rating and the total user ratings of a tv show in the tvshow_cache table, get auto-updated everytime a new rating is added or an existing rating is updated
- The view for the individual tvshow no longer rests inside the layouts folder, but out of it, also there's just one single file for this instead of the previous two, meaning it now utilizes the default layout view that all pages use as their default page structure (which ultimately keeps the code DRY)
- Added an option to add your name to your profile, through the 'Settings' page
- Added a 'Requests' page, where you can view and add requests for tv shows that you want added to the site
- Now any active links on the nav bar will be highlighted
- Clicking on episode titles now reveals episode descriptions (this is done through ajax get method)
- Reduced repetition of js code in the tvshow page using the data attribute property (this repetition was mainly caused due to the use of multiple php 'for' loops)

#0.5.0 (2015-02-15)
- All episodes are automatically added when a show is added to the database through the Add Shows page in the Admin Panel
- Now all database queries that don't really change more than once a day, are cached using Laravel's awesome model caching method (caching is set to store the queries forever, until the cache is cleared manually )
- Everytime a TV Show or an episode is added, the cache is automatically cleared (this prevents the need for the clearing the cache manually everytime anything new is added)
- 'Manage TV Shows Page' has been added to the Admin Panel, for viewing, editing and deleting tvshows(+ everything related to it, with just a single press of a button)
- Ironed out any problems that occur when the database being used is PostgreSQL, and not Sqlite(which I've been using for developing this app until now)
- Now users are able to remove their episode ratings
- The bug that caused wrong episode watch count to be displayed on the 'My Shows' page has now been fixed (thank god I caught this one)
- Slightly simplified routes file
- New 'Watched Shows' page has been added to the application, where if an episode or more has been watched from any tvshow, it will get listed there
- Two extra ways to get episode listing from tvdb & tvrage respectively, have been added to the script (if the first way to get it from tvdb fails, the second one is executed, and then if the second one also fails, the third is executed)
- A 'Settings' page has been added, which allows the users to change their passwords and their email addresses
- Three major additions to the Admin Panel, one is 'Manage Users', the other is 'Manage Roles' and the last one is 'Manage Episodes' (all three are fully functional)
- Corrected the bug in 'Manage TV Shows' page, that caused all the episodes(of other tv shows) in the database to be deleted when one tv show was deleted with its episodes (this was a major bug, if I had missed it, I would've been really f***ed)

##0.4.0 (2015-02-04)
- Users can now rate individual episodes & also change their ratings if they want to, by hitting the same 'save' button used to save watched episodes
- And with that the app can do everything it was meant to do for the users, but there's still a lot to be done and quite a few things that need implementing, so it's far from over, but the hardest parts are done
- The header bar has been rewritten and redesigned, to work on all browsers without flexbox
- A little more css styling was added to the whole design, to make it look a bit nicer
- Now you can use any character you want in your password, there are no limitations, even spaces are allowed (before this, only alphabets, numbers & dashes were allowed, not anymore)
- Slightly resized Login and Registration forms
- Added roles for users, and an admin panel for the user with the role of 'Admin'
- Fully designed admin panel (though most of it doesn't work just yet)
- In the 'My Shows' page, now each show displays an accurate total watched episode count (as opposed to the older dummy value of 125)
- Slightly modified database design
- The issue of the TV rating value not changing on hover, if it doesn't exist in the database, has now been solved, along with other required modifications for it to work properly & perfectly (I didn't even know this issue existed until a day ago)
- Added the option to recover your forgotten password by typing in your email address
- Ability to add a show automatically, just by typing in it's name, which will give you a list of matched shows, from which you'll select a show that matches the one you wanted to add
- The tv show title no longer overflows in the mini-container on pages 'Browse' & 'My Shows', but instead shows an ellipsis if the title is too long to be displayed inside the container width (leaving this bug would really mess up the display of mini-containers if the show titles were too long)
- 'Created by' and 'Actors'(previously called Protagonists) fields on the individual show pages are now filled and not left empty or blank
- Last added shows come first, instead of the other way around (on Browse and My Shows pages)

##0.3.0 (2015-01-29)
- Users can now rate tv shows, and also change their ratings if they want to 
- Ajax is used to prevent page refresh when a show is rated
- The tv rating will be visible even after the page is refreshed, if the person who rated it, is logged in
- Users can now add, save and remove watched episodes(from a TV Show), by selecting them using checkboxes & hitting the save button (this one almost made me lose hope for this app's completion, because it was freaking terrible to implement(non-experienced coder here), it was also very nerve-wrecking, to say the least)

##0.2.0 (2015-01-26)
- Finally, a working 'My Shows' page (every user gets a different page now, as per the shows they've added to their 'My Shows' list)
- Multiple options to add & remove shows to & from the 'My Shows' list
- 'Remember Me' functionality added to the login page
- Now logged in users can't access the registration page or the login page (thank god I remembered)
- A bit more CSS styling was added, to make things look a bit more appealing
- Updated the search bar to look exactly like the photoshop mockup
- Revamped 'Browse' & 'My Shows' pages.
- Header no longer has a full width background, looks much much better
- The footer uses flexbox to be at the very bottom of the page, especially when there's no content above it
- Logout now redirects to the index page, instead of the login page

##0.1.0 (2015-01-23)
- Basic HTML & CSS Design
- Working display of individual TV Show pages, but without any way for the user to do anything on any of them 
- A Working User Management System