<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTvshowTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tvshow', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->integer('show_start_year');
			$table->string('show_end_year')->nullable();
			$table->text('description')->nullable();
			$table->string('cover')->unique();
			$table->integer('seasons');
			$table->string('created_by')->nullable();
			$table->string('actors')->nullable();
			$table->boolean('ongoing')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tvshow');
	}

}
