@extends('layouts.default')

@section('page-title')
Requests
@stop

@section('page-content')

	<div id="browse" class="container container-single clearfix">
		<div class="fake-center">
			<h1>Requests</h1>
			{{ Form::open(['action' => 'RequestsController@create', 'method' => 'get']) }}
				<div class="center"><button class="button button-medium" type="submit"/>Request a new TV Show</button></div>
			{{ Form::close() }}
			<br/>
			<table class="profile-table">
				<thead>
					<th>Request #</th>
					<th>TV Show</th>
					<th>Status</th>
					<th>Requested by</th>
					<th>Created On</th>
					<th>Completed On</th>
				</thead>
				<tbody>
					@foreach($requests as $request)
					<tr>
						<td>{{ $request->id }}</td>
						<td>{{ $request->tvshow_name }}</td>
						<td>@if($request->completed == TRUE) Closed @else Open @endif</td>
						<?php $username = User::find($request->uid)->username; ?>
						<td><a href="{{ URL::to('/') }}/profile/{{ $username }}">{{ $username }}</a></td>
						<td>{{ $request->created_at->setTimezone($timezone)->format('d-M-Y h:m A') }}</td>
						<td>@if(empty($request->completed_on)) N/A @else {{ $request->completed_on->setTimezone($timezone)->format('d-M-Y h:m A') }} @endif</td>
						@if(Auth::check())
							@if(Auth::id() == $request->uid || Auth::user()->isAdmin())
								{{ Form::open(['action' => ['RequestsController@edit', $request->id], 'method' => 'get']) }}
								<td><button class="button" type="submit">Edit</button></td>
								{{ Form::close() }}
							@endif
							@if( Auth::user()->isAdmin() )
								@if($request->completed == FALSE)
								{{ Form::open(['action' => ['RequestsController@close', $request->id], 'method' => 'patch']) }}
								<td><button class="button" type="submit">Close</button></td>
								{{ Form::close() }}
								@else
								{{ Form::open(['action' => ['RequestsController@reopen', $request->id], 'method' => 'patch']) }}
								<td><button class="button" type="submit">Reopen</button></td>
								{{ Form::close() }}
								@endif
								{{ Form::open(['action' => ['RequestsController@destroy', $request->id], 'method' => 'delete']) }}
								<td><button class="button" type="submit">Delete</button></td>
								{{ Form::close() }}
							@endif
						@endif
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

@stop