<?php

class ManageEpisodesController extends \BaseController {

	// this method exists to keep the code DRY in this controller
	// though I don't know if putting it here is the best practice
	public function sort()
	{
		if(!isset($_GET['show']))
				$sort_me = NULL;
			else 
				$sort_me = $_GET['show'];

		switch($sort_me) {
			case 'ended': 
			$tvshow = TVShow::where('ongoing','=', 0)->orderBy('id','DESC')->get();
			break;
			case 'ongoing':
			$tvshow = TVShow::where('ongoing','=', 1)->orderBy('id','DESC')->get();
			break;
			case 'all':
			$tvshow = TVShow::orderBy('id','DESC')->get();
			break;
			default:
			$tvshow = TVShow::orderBy('id','DESC')->get();
		}
		return $tvshow;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tvshow = $this->sort();
		return View::make('admin.manage-episodes.index', compact('tvshow'));
	}

	public function getTotalSeasons()
	{
		$id = Input::get('id');
		$tvshow = TVShow::find($id);
		$total_seasons = $tvshow->seasons;
		return $total_seasons;
	}

	public function getEpisodeListing()
	{
		$tvid = Input::get('tv_id');
		$season = Input::get('tv_season');
		$episodes = Episode::where('tvid','=',$tvid)->where('season','=', $season)->get();
		//return $episodes;
		return json_encode( $episodes );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$tvshow = $this->sort();
		return View::make('admin.manage-episodes.add-episode', compact('tvshow'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$tvid = Input::get('tvid');
		$season = Input::get('season');
		$episode_title = Input::get('episode_name');
		$episode_number = Input::get('episode_number');
		$episode_description = Input::get('description');
		$episode_aired_on = Input::get('aired_on');

		if(Helper::checkNotEmpty($tvid, $season, $episode_title, $episode_number, $episode_description, $episode_aired_on))
		{
			$add_episode = new Episode;
			$add_episode->tvid = $tvid;
			$add_episode->season = $season;
			$add_episode->title = $episode_title;
			$add_episode->number = $episode_number;
			$add_episode->description = $episode_description;
			$add_episode->aired_on = $episode_aired_on;
			$add_episode->save();
			return Redirect::action('ManageEpisodesController@index')->withSuccess('A new episode has been added to Season '.$season.' of '.TVShow::find($tvid)->title);
		}
		else
			return Redirect::back()->withInput()->withError('Please complete the form, you cannot submit a half filled form!');
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$episode = Episode::with('tvshow')->find($id);
		return View::make('admin.manage-episodes.edit-episode', compact('episode'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$episode_number = Input::get('episode_number');
		$episode_name = Input::get('episode_name');
		$episode_aired_on = Input::get('aired_on');
		$episode_description = Input::get('description');

		$episode = Episode::find($id);
		if(!empty($episode_number))
			$episode->number = $episode_number;
		if(!empty($episode_name))
			$episode->title = $episode_name;
		if(!empty($episode_aired_on))
			$episode->aired_on = $episode_aired_on;
		if(!empty($episode_description))
			$episode->description = $episode_description;
		$episode->save();

		Cache::flush(); //clear cache to reflect changes

		return Redirect::action('ManageEpisodesController@index')->with('success', 'The selected episode has been successfully updated');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Episode::find($id)->delete();
		return Redirect::back()->withSuccess('The selected episode has been successfully deleted!');
	}


}
