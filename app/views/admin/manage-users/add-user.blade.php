@extends('admin.default-layout')

@section('extra-title','Add User')

@section('section-content')
<h1>Add User</h1>
<div id="form" class="clearfix">
	{{ Form::open(array('action' => 'ManageUsersController@store')) }}
	<label for="username" class="set-display-block">Username:</label>
	<input placeholder="Username" class="set-display-block" name="username" type="text" required />
	<label for="password" class="set-display-block">Password:</label>
	<input placeholder="Password" class="set-display-block" name="password" type="text" required />
	<label for="email" class="set-display-block">Email Address:</label>
	<input placeholder="Email Address" class="set-display-block" name="email" type="text" required />
	<label for="role" class="set-display-block">Role:</label>
	{{ Form::select('role', $roles) }}
	<br/><br/>
	{{ Form::button('Add User', array('class'=>'button button-medium button-input-width', 'type'=>'submit')) }}
	{{ Form::close() }}
</div>
@stop