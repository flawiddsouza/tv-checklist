<?php

class TVShowCache extends Eloquent {

	protected $table = 'tvshow_cache';

	protected $fillable = array('tvid', 'total_episodes', 'avg_rating', 'total_user_ratings', 'total_watch_count');
	
}