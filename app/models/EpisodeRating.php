<?php

class EpisodeRating extends Eloquent {

	protected $table = 'episode_rating';

	protected $fillable = array('uid', 'tvid', 'season', 'episode', 'rating');
	
}