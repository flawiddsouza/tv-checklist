@extends('layouts.default')

@section('page-title')
@yield('extra-title') - Admin Panel
@stop

@section('extra-head-includes')
<link rel="stylesheet" href="{{asset('static/css/admin.css')}}" />
@stop

@section('page-content')
	@if(Session::has('success'))
		<p class="center alert alert-success">{{ Session::get('success') }}</p>
	@endif

	@if(Session::has('error'))
		<p class="center alert alert-error">{{ Session::get('error') }}</p>
	@endif
	<div class="admin-container">
		<aside>
			<ul>
				<li>
					<ul>
						<a href="{{ URL::action('AdminController@index') }}">
							<li class="@if(Request::url() == URL::action('AdminController@index')){{'active'}}@endif">Dashboard</li>
						</a>
					</ul>
				</li>
				<li>
					<ul>
						<a href="{{ URL::action('AdminController@AddShows') }}">
							<li class="@if(Request::url() == URL::action('AdminController@AddShows')){{'active'}}@endif">Add TV Shows</li>
						</a>
						<a href="{{ URL::action('ManageTVShowsController@index') }}">
							<li class="@if(Request::is('admin/manageshows*')){{'active'}}@endif">Manage TV Shows</li>
						</a>
					</ul>
				</li>
				<li>
					<ul>
						<a href="{{ URL::action('ManageEpisodesController@index') }}">
							<li class="@if(Request::is('admin/manageepisodes*')){{'active'}}@endif">Manage Episodes</li>
						</a>
					</ul>
				</li>
				<li>
					<ul>
						<a href="{{ URL::action('ManageUsersController@index') }}">
							<li class="@if(Request::is('admin/manageusers*')){{'active'}}@endif">Manage Users</li>
						</a>
						<a href="{{ URL::action('RolesController@index') }}">
							<li class="@if(Request::is('admin/manageroles*')){{'active'}}@endif">Manage Roles</li>
						</a>
					</ul>
				</li>
			</ul>
		</aside>
		<section>
			@yield('section-content')
		</section>
	</div>
	@yield('footer-includes')
@stop