@extends('layouts.default')

@section('page-title')
Forgot Password
@stop

@section('page-content')
	@if(Session::has('status'))
		<p class="center alert alert-success">{{ Session::get('status') }}</p>
	@endif

	@if(Session::has('error'))
		<p class="center alert alert-error">{{ Session::get('error') }}</p>
	@endif
	<div class="container container-single center">
		<div id="form">
			{{ Form::open(array('action' => 'RemindersController@postRemind', 'method' => 'POST')) }}
				<h1>Forgot Password</h1>

				{{ Form::email('email', null, array('class'=>'set-display-block', 'placeholder'=>'Email')) }}
				
				{{ Form::button('Forgot Password', array('class'=>'button', 'type'=>'submit')) }}</div>
				
			{{ Form::close() }}
		</div>
	</div>
@stop