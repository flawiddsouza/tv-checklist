<?php

class WatchedTVShow extends Eloquent {

	protected $table = 'watched_tvshow';

	protected $fillable = array('uid', 'tvid');
	
	//used for the 'My Shows' page
	public function tvshow() {
		return $this->belongsTo('TVShow', 'tvid');
	}

}