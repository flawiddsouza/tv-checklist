@extends('layouts.default')

@section('page-title')
Request a TV Show
@stop

@section('page-content')

	<div id="browse" class="container container-single clearfix">
		<div class="fake-center center">
			<div id="form" class="clearfix">
				<h1>Create Request</h1>
				@if($errors->any())
				<ul>
					@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
				@endif
				{{ Form::open(['action' => 'RequestsController@store']) }}
				{{ Form::text('tvshow_name', null, ['placeholder' => 'Name of the TV Show *', 'class' => 'set-display-block']) }}
				{{ Form::number('first_aired_year', null, ['placeholder' => 'First Aired Year *', 'class' => 'set-display-block']) }}
				{{ Form::text('imdb_id', null, ['placeholder' => 'IMDB ID or Link to the IMDB Page', 'class' => 'set-display-block']) }}
				<p>{{ Form::button('Create Request', ['type' => 'submit', 'class' => 'button button-medium']) }}</p>
				{{ Form::close() }}
			</div>
		</div>
	</div>

@stop