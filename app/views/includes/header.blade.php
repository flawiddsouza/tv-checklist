<header>
	<div id="logo">
		<a href="{{ URL::to('/') }}"><img src="{{asset('static/img/logo.png')}}" alt="TV Checklist"/></a>
	</div>
	<div id="search-bar">
		<form action="{{ action('SearchController@index') }}">
			<input name='q' @yield('search-value') type="search" required placeholder="Search..." />
			<button type="submit"><img width="25" height="25" src="{{asset('static/img/search-icon.svg')}}" alt=""></button>
		</form>
	</div>
	<div id="right">
		<span @if(Request::is('requests*'))class="highlight-green"@endif>{{ HTML::linkAction('RequestsController@index','Request a TV Show') }}</span>
	</div>
</header>
<nav class="container center">
	<ul>
		@if(Auth::check())
		<li>Hello, <a href="{{ URL::action('UserController@showProfile', Auth::user()->username) }}">@if(empty(Auth::user()->name)) {{ Auth::user()->username }} @else {{ Auth::user()->name }} @endif</a>
			@if( Auth::user()->isAdmin() )
				{{ HTML::linkAction('AdminController@index','(Admin Panel)') }}
			@endif
		</li>
		@endif
		@if(!Auth::check())
			<li><span @if(Request::is('browse*'))class="highlight-green"@endif>{{ HTML::linkAction('BrowseController@showBrowse','Browse') }}</span></li>
			<li><span @if(Request::is('register'))class="highlight-green"@endif>{{ HTML::linkAction('UserController@showRegister','Register') }}</span></li>  
			<li><span @if(Request::is('login'))class="highlight-green"@endif>{{ HTML::linkAction('UserController@showLogin','Login') }}</span></li> 
		@else
			<li><span @if(Request::is('myshows'))class="highlight-green"@endif>{{ HTML::linkAction('UserController@showMyShows','My Shows') }}</span></li>
			<li><span @if(Request::is('watchedshows'))class="highlight-green"@endif>{{ HTML::linkAction('UserController@WatchedShows','Watched Shows') }}</span></li>
			<li><span @if(Request::is('browse*'))class="highlight-green"@endif>{{ HTML::linkAction('BrowseController@showBrowse','Browse') }}</span></li>
			<li><span @if(Request::is('settings'))class="highlight-green"@endif>{{ HTML::linkAction('UserController@showSettings','Settings') }}</span></li>
			<li><li>{{ HTML::linkRoute('logout','Logout') }}</li></li>
        @endif
	</ul>
</nav>
@if( Auth::check() && Auth::user()->isAdmin() )
<nav class="container center">
	<ul>
		Quick Links:
		<li><a href="{{ URL::action('AdminController@AddShows') }}">Add Shows</a></li>
	</ul>
</nav>
@endif