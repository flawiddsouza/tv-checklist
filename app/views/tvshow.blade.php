@extends('layouts.default')

@section('page-title') 
{{ $tvshow->title }} ({{ $tvshow->show_start_year }}–{{ $tvshow->show_end_year }})
@stop

@section('meta-description', htmlentities($tvshow->description)) 

@section('extra-head-includes')
	<script src="{{asset('static/js/jquery.idTabs.min.js')}}"></script>
	<script src="{{asset('static/js/spin.min.js')}}"></script>
@stop

@section('page-content')
	@if(Session::has('success_add'))
		<p class="center alert alert-success">{{ Session::get('success_add') }}</p>
	@endif
	@if(Session::has('success_remove'))
		<p class="center alert alert-remove">{{ Session::get('success_remove') }}</p>
	@endif	
	
	@include('partials.tvpage.tv-data-display')

	@include('partials.tvpage.season-episode-display')
	
<script type="text/javascript"> 
$("#loading").hide();
//for the tabs
$("#usual1 ul").idTabs(); 

//for the spinner
var opts = {
  lines: 13, // The number of lines to draw
  length: 20, // The length of each line
  width: 10, // The line thickness
  radius: 30, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 0, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#000', // #rgb or #rrggbb or array of colors
  speed: 1, // Rounds per second
  trail: 60, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: false, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: '10%', // Top position relative to parent
  left: '50%' // Left position relative to parent
};
var target = document.getElementById('loading');
var spinner = new Spinner(opts).spin(target);

//get episode description
$( ".get-desc" ).click(function(e) {
	e.preventDefault();
	var tvid = $(this).data('tvid');
	var season = $(this).data('season');
	var episode = $(this).data('episode');
	$.get( '{{ URL::to('/') }}/browse/series/'+tvid+'/'+season+'/'+episode, function( data ) {
		$( ".desc-clear" ).html( "" );
		$( ".desc-clear" ).toggle();
		$( ".desc"+season+episode ).html( '<div class="add-box">'+data+' <a class="hide-desc button button-tiny" href="#">Hide</a></div>' );
		$( ".desc-clear" ).toggle();
	});
});

//hide the open description
$( ".desc-clear" ).on('click', '.hide-desc', function(e) {
	e.preventDefault();
	$( ".desc-clear" ).html( "" );
});


//for the tv show rating
$(document).ready(function() {
	@if($rating != 0)
	//changes the value of the rating to the hovered element
	$('#show_rating input[type="radio"]').mouseenter(function(){
		var hoverme = $(this).val();
		$('span#my-rating').replaceWith("<span id='my-rating'>"+hoverme+"</span>");
	});
	//changes the rating back to the selected element
	$('#show_rating input[type="radio"]').mouseleave(function(){
		var hoverme = $("input[type=radio]:checked").val();
		$('span#my-rating').replaceWith("<span id='my-rating'>"+hoverme+"</span>");
	});

	@elseif($rating == 0)
	//changes the value of the rating to the hovered element
	$('#show_rating input[type="radio"]').mouseenter(function(){
		var hoverme = $(this).val();
		var rating = $("input[type=radio]:checked").val();
		if(rating === undefined || rating === null)
			$('span#before-rating').replaceWith("<span id='my-rating'>"+hoverme+"</span>");
		else
			$('span#my-rating').replaceWith("<span id='my-rating'>"+hoverme+"</span>");
	});
	//changes the rating back to none
	$('#show_rating input[type="radio"]').mouseleave(function(){
		var rating = $("input[type=radio]:checked").val();
		if(rating === undefined || rating === null)
			$('span#my-rating').replaceWith("<span id='before-rating' class='dark-grey'>-</span>");
		else 
			$('span#my-rating').replaceWith("<span id='my-rating'>"+rating+"</span>");
	});
	@endif

	$('#show_rating input[type="radio"]').click(function(){
		var item = $("input[type=radio]:checked").val();
		if ($(this).is(':checked'))
		{
		@if(Auth::check())
			var $form = $( 'form#show_rating' ),
			url = $form.attr( "action" );
			$.post( url, { rating: item } );
			$('span#show_rating').html("<span id='my-rating'>"+item+"</span>");
		@else
			$('form#show_rating').submit();
		@endif
		}
	});

});

//for selecting all checkboxes per season
$(document).ready(function() {
	$('.selectall').click(function(event) {  //on click 
		var season = $(this).data('season');
		if(this.checked) { // check select status
			$('.checkbox'+season).each(function() { //loop through each checkbox
			this.checked = true;  //select all checkboxes with class "checkbox<season_no>"               
			});
		}
		else{
			$('.checkbox'+season).each(function() { //loop through each checkbox
			this.checked = false; //deselect all checkboxes with class "checkbox<season_no>"                       
			});
		}
	});
});

//for adding+removing watched episodes & for adding+remove ratings for episodes
$(document).ready(function() {
@for($season = 1; $season <= $tvshow->seasons; $season++)
	$( "form#add_watched_episode{{$season}}" ).submit(function( event ) {
		@if(Auth::check())
			event.preventDefault();

			$('#loading').show();

			var watched = [];
			var ep_id_arr = [];
			$("form input[class='checkbox{{$season}}']").each(function () {
				if ($(this).is(':checked')) {
					watched.push(1);
					var ep_id = $(this).val();
					ep_id_arr.push(ep_id);
				}
				else {
					watched.push(0);
					ep_id_arr.push(0);
				}
			});

			var rate = [];
			for(var i=1; i<={{ $ep_count[$season] }}; i++){
				var item = $('input[name=rate{{$season}}'+i+']:checked').val();
				if(item === undefined || item === null) {
					rate.push(0);
				} else {
					rate.push(item);
				}
			}

			var $form = $( 'form#add_watched_episode{{$season}}' ),
			url = $form.attr( "action" );
			$.post( url, { formData: watched, EpIdArray: ep_id_arr, Rating: rate }) .done(function() { alertify.set({ delay: 3000 }); alertify.success("Saved!"); }) .complete(function(){ $('#loading').hide(); });
		@else
			$('form#add_watched_episode{{$season}}').submit();
		@endif
	});
@endfor

	$(".remove-episode-rating a").click(function(e) {
		e.preventDefault();
		var season = $(this).data('season');
		var episode = $(this).data('episode');
		$('input[name=rate'+season+episode+']:checked').prop('checked', false);
	});

});

</script>
@stop