<?php

class TVShowGenre extends Eloquent {

	protected $table = 'tvshow_genre';

	protected $fillable = ['tvid', 'genre_id'];

}