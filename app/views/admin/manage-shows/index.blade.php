@extends('admin.default-layout')

@section('extra-title','Manage TV Shows')

@section('section-content')
<h1>Manage TV Shows</h1>
<div class="sort">Show: <a href="?show=ongoing" class="button" />Ongoing</a> <a href="?show=ended" class="button" />Ended</a> <a href="?show=all" class="button" />All</a></div>
<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>TV Show</th>
			<th>Seasons</th>
			<th>Episodes</th>
			<th>Avg. Rating</th>
			<th>Ratings</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		@foreach($tvshow as $show)
		<tr>
			<td>{{ $show->id }}</td>
			<td>{{ $show->title }}</td>
			<td>{{ $show->seasons }}</td>
			<td>{{ $show->cache->total_episodes }}</td>
			<td>{{ $show->cache->avg_rating }}
			<td>{{ $show->cache->total_user_ratings }}</td>
			<td>@if($show->ongoing) On @else Ended @endif</td>
			{{ Form::open(['action' => ['ManageTVShowsController@show', $show->id], 'method' => 'get']) }}
			<td><button class='button'>View</button></td>
			{{ Form::close() }}
			{{ Form::open(['action' => ['ManageTVShowsController@edit', $show->id], 'method' => 'get']) }}
			<td><button class='button' type='submit'>Edit</button></td>
			{{ Form::close() }}
			{{ Form::open(['id' => 'delete', 'action' => ['ManageTVShowsController@destroy', $show->id], 'method' => 'delete']) }}
			<td><button class='button' type='submit'>Delete</button></td>
			{{ Form::close() }}
		</tr>
		@endforeach
	</tbody>
</table>	
@stop

@section('footer-includes')
<script>
$('form#delete').submit(function() {
	var c = confirm("Are you sure you want to delete this tvshow? Know that deleting the tv show will also delete all its episodes as well as any user data associated with it!");
	return c; //you can just return c because it will be true or false
});
</script>
@stop