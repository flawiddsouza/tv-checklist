@extends('layouts.default')

@section('page-title')
Search Results for {{ $search_term }}
@stop

@section('search-value', 'value="'.$search_term.'"')

@section('page-content')

	<div id="browse" class="container container-single clearfix">
		<div class="fake-center">
			<h1>Showing results containing "{{ $search_term }}"</h1>
			@if(!$query->isEmpty())
			<table class="flat-table">
				@foreach($query as $result)
				<tr>
					<td><a href="/browse/series/{{ $result->id }}/{{ str_replace(' ','-', $result->title) }}">{{ $result->title }} ({{$result->show_start_year }})</a></td>
				</tr>
				@endforeach
			</table>
			@else
			<div class="fill-margin center">No results found for "{{ $search_term }}"</div>
			@endif
		</div>
	</div>

@stop