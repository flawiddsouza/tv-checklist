<title>@yield('page-title') - TV Checklist</title>
<meta charset="utf-8"> 
<meta name="description" content="@yield('meta-description', "TV Checklist helps you track all the tv shows and episodes you've watched and the ones you're currently watching, using checklists.")">
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link href="{{asset('static/css/base.css')}}" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="{{asset('static/js/html5shiv.min.js')}}"></script>
<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
if (typeof jQuery == 'undefined') {
	document.write(unescape("%3Cscript src='{{asset('static/js/jquery-1.11.2.min.js')}}' type='text/javascript'%3E%3C/script%3E"));
}
</script>