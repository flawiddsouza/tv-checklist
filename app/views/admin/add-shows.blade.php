@extends('admin.default-layout')

@section('extra-title','Add Shows')

@section('section-content')
<h1>Add Shows</h1>
@if(empty($series_name) && empty($seriesid))
<div id="form" class="clearfix">
	{{ Form::open(array('method' => 'GET')) }}
	{{ Form::text('search', null, array('class'=>'set-display-block', 'placeholder'=>'Name of the TV Show to be added')) }}
	{{ Form::button('Get TV Data and Episode Listing for this Show', array('class'=>'button', 'type'=>'submit')) }}
	{{ Form::close() }}
</div>
@elseif(empty($seriesid))
<p><strong>Please select the one you're looking for, from the list below:</strong></p><br/>
@foreach($SeriesIDUsingTitle->Series as $series)
<a href="?seriesid={{ $series->seriesid }}">{{ $series->SeriesName }}</a><br/>
{{ $series->Overview }}<br/><br/>
@endforeach
@endif
@if(!empty($seriesid))
<div id="form" class="clearfix">
	{{ Form::open(array('action' => 'AdminController@doAddShows')) }}
	{{ Form::label('tvshow_name', 'TV Show Name:', array('class'=>'set-display-block')) }}
	{{ Form::text('tvshow_name', $tvshow_name, array('placeholder'=>'TV Show Name')) }}
	{{ Form::label('start_year', 'Start Year:', array('class'=>'set-display-block')) }}
	{{ Form::text('start_year', $tvshow_start_year, array('placeholder'=>'Start Year')) }}
	{{ Form::label('end_year', 'End Year:', array('class'=>'set-display-block')) }}
	{{ Form::text('end_year', $getEndYear, array('placeholder'=>'End Year (Leave Blank if None)')) }}
	{{ Form::label('description', 'Description:', array('class'=>'set-display-block')) }}
	{{ Form::textarea('description', $getDescription, array('placeholder'=>'Description')) }}
	{{ Form::label('cover_upload', 'Cover Image URL:', array('class'=>'set-display-block')) }}
	{{ Form::text('cover_upload', $getPoster, array('placeholder'=>'Cover Image URL')) }}
	<br/>
	<p>Image Preview:</p>
	<img id="cover" src=""/><br/>
	{{ Form::label('total_seasons', 'Total Seasons:', array('class'=>'set-display-block')) }}
	{{ Form::text('total_seasons', $total_seasons, array('placeholder'=>'Total Seasons')) }}
	{{ Form::label('total_episodes', 'Total Episodes:', array('class'=>'set-display-block')) }}
	{{ Form::text('total_episodes', $total_episodes, array('placeholder'=>'Total Episodes')) }}	
	{{ Form::label('created_by', 'Creator:', array('class'=>'set-display-block')) }}
	{{ Form::text('created_by', $getCreator, array('placeholder'=>'Created by')) }}
	{{ Form::label('actors', 'Actors:', array('class'=>'set-display-block')) }}
	{{ Form::text('actors', $getActors, array('placeholder'=>'Actors')) }}
	{{ Form::label('imdb_id', 'IMDB ID:', array('class'=>'set-display-block')) }}
	{{ Form::text('imdb_id', $tvshow_imdb_id, array('placeholder'=>'IMDB ID')) }}
	{{ Form::label('tvdb_id', 'TVDB ID:', array('class'=>'set-display-block')) }}
	{{ Form::text('tvdb_id', $seriesid, array('placeholder'=>'TVDB ID')) }}
	{{ Form::label('tvrage_id', 'TVRage ID:', array('class'=>'set-display-block')) }}
	{{ Form::text('tvrage_id', $TVrageID, array('placeholder'=>'TVRage ID')) }}
	{{ Form::label('current_retrieval_time', 'Current Retrieval Time:', array('class'=>'set-display-block')) }}
	{{ Form::text('current_retrieval_time', $current_time, array('class'=>'set-display-block', 'placeholder'=>'Current Retrieval Time')) }}
</div>
<div id="addshows">
	<table>
		<tr>
			<td>E</td>
			<td>Name</td>
			<td>S</td>
			<td>Aired</td>
			<td>Description</td>
		</tr>
		@for($i=0; $i<$total_episodes; $i++)
		<tr>
			<td>{{ Form::text('episode_number[]', $episode_number[$i]) }}</td>
			<td>{{ Form::text('episode_name[]', $episode_name[$i]) }}</td>
			<td>{{ Form::text('episode_season[]', $episode_season[$i]) }}</td>
			<td>{{ Form::text('episode_aired_on[]', $episode_aired_on[$i]) }}</td>
			<td>{{ Form::text('episode_description[]', $episode_description[$i]) }}</td>
		</tr>
		@endfor
	</table>
</div>
<div id="form" class="clearfix">
	<br/>
	{{ Form::button('Save', array('class'=>'button button-medium button-input-width', 'type'=>'submit')) }}
	{{ Form::close() }}
</div>
@endif
@stop

@section('footer-includes')
<script>
//displays the image from the url that is pre-entered into the form field
$("#cover").attr("src", $("input[name=cover_upload]").val());
//if the image url changes then the below method is called
$("input[name=cover_upload]").on('change', function() {
	var cover = $("input[name=cover_upload]").val();
	$("#cover").attr("src", cover);
});
</script>
@stop