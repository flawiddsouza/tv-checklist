<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

	protected $fillable = array('username', 'password', 'email', 'role', 'name', 'ip', 'timezone');

	public static $rules = [
	    'username' => 'required|alpha|min:4|unique:user',
	    'email' => 'required|email|unique:user',
	    'password' => 'required|min:6|confirmed',
	    'password_confirmation' => 'required|min:6',
	    'timezone' => ['regex:/^([A-Za-z\/]*)$/']
    ];
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	//Admin checker
	public function isAdmin()
	{
		return $this->role == '1';
	}

	//Used by the Browse page & the user profile page
	public function shows()
	{
		return $this->belongsToMany('TVShow', 'watched_tvshow', 'uid', 'tvid');
	}

}