@extends('layouts.default')

@section('page-title')
My Shows
@stop

@section('page-content')

	@if(Session::has('success'))
		<p class="center alert alert-success">{{ Session::get('success') }}</p>
	@endif

	@if(Session::has('error'))
		<p class="center alert alert-error">{{ Session::get('error') }}</p>
	@endif

	<div id="browse" class="container container-single clearfix">
		<div class="fake-center">
		<h1>My Shows</h1>
			@if($watched_tvshow->isEmpty())
			<div class="fill-margin center">Currently there are no shows in your list. Add some by going {{ HTML::linkAction('BrowseController@showBrowse','here') }} or you can use the search bar to do the same.</div>
			@else
			{{ Form::open(array('url' => 'myshows')) }}
			<div class="remove-show">{{ Form::button('Remove from My Shows', array('class'=>'button button-medium', 'type'=>'submit')) }}</div>
				@foreach($watched_tvshow as $show)
				<div class="mini-container">
					<div><p title="{{ $show->tvshow->title }}" class="ellipsis"><strong>{{ $show->tvshow->title }}</strong></p> {{ Form::checkbox('select'.$show->id.'', $show->id) }}</div>
					<a href="/browse/series/{{ $show->tvshow->id }}/{{ str_replace(' ','-', $show->tvshow->title) }}">{{ HTML::image($covers_directory.'/'.$show->tvshow->cover) }}</a>
					<div>Watched <strong>{{ WatchedEpisode::where('tvid','=',$show->tvshow->id)->where('uid','=',Auth::id())->count() }}</strong> episodes</div>
				</div>
				@endforeach
			{{ Form::close() }}
			@endif
		</div>
	</div>

@stop