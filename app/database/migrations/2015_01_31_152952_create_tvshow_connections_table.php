<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTvshowConnectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tvshow_connections', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tvid');
			$table->string('imdb_id')->nullable();
			$table->integer('tvdb_id')->nullable();
			$table->integer('tvrage_id')->nullable();
			$table->integer('last_retrieval_time')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tvshow_connections');
	}

}
