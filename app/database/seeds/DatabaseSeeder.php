<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// Create the first user and make him the admin
		User::create([
			'username' => 'flawiddsouza',
			'password' => Hash::make('xd01820wI#K4E$c1GnCY'),
			'role' => 1,
			'email' => 'flawid@live.in'
		]);

		// Create two default roles
		Role::create([
			'name' => 'Admin' 
		]);

		Role::create([
			'name' => 'User' 
		]);
		// $this->call('UserTableSeeder');
	}

}
