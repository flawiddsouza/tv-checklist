<?php

class RequestsController extends \BaseController {

	public function __construct()
	{
		$this->beforeFilter('auth', array('except' => 'index'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$requests = Requests::latest()->paginate(20);

		if(Auth::check())
		{
			if(Auth::user()->timezone != NULL)
				$timezone = Auth::user()->timezone;
			else
				$timezone = 'UTC';
		}
		else
			$timezone = 'UTC';

		return View::make('requests.index', compact('requests', 'timezone'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('requests.add-request');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{		
		$current_year = Carbon\Carbon::now()->year;
		
		$inputs = Input::all();
		$validator = Validator::make(Input::all(), ['tvshow_name' => 'required|max:55', 'first_aired_year' => 'required|numeric|min:1959|max:'.$current_year.'']);
		if($validator->passes())
		{
			$inputs['uid'] = Auth::id();
			$inputs['completed'] = 0;
			Requests::create($inputs);
			return Redirect::action('RequestsController@index');
		}
		else
			return Redirect::back()->withErrors($validator)->withInput();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try {
			$request = Requests::findorFail($id);
		} 

		catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			return Redirect::action('RequestsController@index');
		}

		if($request->uid == Auth::id() || Auth::user()->isAdmin())
			return View::make('requests.edit-request', compact('request'));
		else
			return Redirect::action('RequestsController@index');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		try {
			$request = Requests::findorFail($id);
		} 

		catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			return Redirect::action('RequestsController@index');
		}

		if($request->uid == Auth::id() || Auth::user()->isAdmin())
		{
			$current_year = Carbon\Carbon::now()->year;
			
			$inputs = Input::all();
			$validator = Validator::make(Input::all(), ['tvshow_name' => 'required|max:55', 'first_aired_year' => 'required|numeric|min:1959|max:'.$current_year.'']);
			if($validator->passes())
			{
				Requests::find($id)->update($inputs);
				return Redirect::action('RequestsController@index');
			}
			else
				return Redirect::back()->withErrors($validator)->withInput();
		}
		else
			return Redirect::action('RequestsController@index');
	}

	public function close($id)
	{
		Requests::find($id)->update(['completed' => 1, 'completed_on' => Carbon\Carbon::now()]);
		return Redirect::back();
	}

	public function reopen($id)
	{
		Requests::find($id)->update(['completed' => 0, 'completed_on' => NULL]);
		return Redirect::back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Requests::find($id)->delete();
		return Redirect::back();
	}


}
