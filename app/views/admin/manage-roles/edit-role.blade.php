@extends('admin.default-layout')

@section('extra-title','Edit Role')

@section('section-content')
<h1>Edit Role</h1>
<div id="form" class="clearfix">
	{{ Form::open(array('action' => ['RolesController@update', $role->id], 'method' => 'patch')) }}
	{{ Form::label('role_name', 'Role Name:', array('class'=>'set-display-block')) }}
	{{ Form::text('role_name', $role->name, array('placeholder'=>'Role Name')) }}
	<br/>
	{{ Form::button('Update Role', array('class'=>'button button-medium button-input-width', 'type'=>'submit')) }}
	{{ Form::close() }}
</div>
@stop