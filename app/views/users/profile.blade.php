@extends('layouts.default')

@section('page-title')
{{ $user_profile->username }}
@stop

@section('page-content')

	<div id="browse" class="container container-single">
		<div class="fake-center center">
			<img class="profile-image" src="https://s3.amazonaws.com/uifaces/faces/twitter/tofslie/73.jpg"/>
			<h2><a href="{{ URL::to('/') }}/profile/{{ $user_profile->username }}">@if(empty($user_profile->name)) {{ $user_profile->username }} @else {{ $user_profile->name }} @endif</a></h2>
			Has watched a total of <span class="highlight-golden">{{ $total_watched_shows }}</span> @if($total_watched_shows == 1) TV Show @else TV Shows @endif and <span class="highlight-orange">{{ $total_watched_episodes }}</span> @if($total_watched_episodes == 1) Episode. @else Episodes. @endif
		</div>
	</div>

	<div class="center">
		<div id="browse" class="container container-single container-half set-display-inline-block">
			<div class="fake-center center">
				<h3>My Shows</h3>
				@if($user_profile->shows->isEmpty())
				{{ $user_profile->username }} doesn't seem to have added any tv shows to the watchlist.
				@else
				<table class="profile-table" data-sortable>
					<thead>
						<th>TV Show</th>
						<th>Watched Episodes</th>
					</thead>
					<tbody>
				@foreach($user_profile->shows as $user_MyShow)
						<tr>
							<td><a href="/browse/series/{{ $user_MyShow->id }}/{{ str_replace(' ','-', $user_MyShow->title) }}">{{ $user_MyShow->title}}</a></td>
							<td>{{ WatchedEpisode::where('tvid','=', $user_MyShow->id)->where('uid','=', $user_profile->id)->count() }}</td>
						</tr>
				@endforeach
					</tbody>
				</table>
				@endif
			</div>
		</div>

		<div id="browse" class="container container-single container-half set-display-inline-block">
			<div class="fake-center center">
				<h3>Watched Shows</h3>
				@if($watched_tvshows->isEmpty())
				{{ $user_profile->username }} doesn't seem to have watched any tv shows or episodes.
				@else
				<table class="profile-table" data-sortable>
					<thead>
						<th>TV Show</th>
						<th>Watched Episodes</th>
					</thead>
					<tbody>
				@foreach($watched_tvshows as $watched_tvshow)
						<tr>
							<td><a href="/browse/series/{{ $watched_tvshow->id }}/{{ str_replace(' ','-', $watched_tvshow->title) }}">{{ $watched_tvshow->title}}</a></td>
							<td>{{ WatchedEpisode::where('tvid','=', $watched_tvshow->id)->where('uid','=', $user_profile->id)->count() }}</td>
						</tr>
				@endforeach
					</tbody>
				</table>
				@endif
			</div>
		</div>

		<div id="browse" class="container container-single">
			<div class="fake-center center">
				<h3>Recently Watched Episodes (Last 10)</h3>
				@if($watched_episodes->isEmpty())
				{{ $user_profile->username }} doesn't seem to have watched any episodes as of yet.
				@else
				<table class="profile-table" data-sortable>
					<thead>
						<th>TV Show</th>
						<th>Season</th>
						<th>Episode No.</th>
						<th>Watched Episode</th>
						<th>Marked as Watched on</th>
					</thead>
					<tbody>
				@foreach($watched_episodes as $watched_episode)
						<tr>
							<td><a href="/browse/series/{{ $watched_episode->tvid }}/{{ str_replace(' ','-', $watched_episode->tvshow->title) }}">{{ $watched_episode->tvshow->title }}</td>
							<td>{{ $watched_episode->episode_data->season }}</td>
							<td>{{ $watched_episode->episode_data->number }}</td>
							<td>{{ $watched_episode->episode_data->title }}</td>
							<td>{{ $watched_episode->created_at->format('d-M-Y') }}</td>
						</tr>
				@endforeach
					</tbody>
				</table>
				@endif
			</div>
		</div>
	</div>
@stop