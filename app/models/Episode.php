<?php

class Episode extends Eloquent {

    protected $table = 'episode';
    
    protected $fillable = array('tvid', 'season', 'title', 'number', 'aired_on');

	//Used for an administrative task (here to display the title of the tvshow on the edit episode page)
	public function tvshow()
	{
		return $this->belongsTo('TVShow', 'tvid');
	}

}