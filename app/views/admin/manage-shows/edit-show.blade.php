@extends('admin.default-layout')

@section('extra-title','Edit Show')

@section('section-content')
<h1>Edit {{ $tvshow->title }} ({{ $tvshow->show_start_year }} - {{ $tvshow->show_end_year }})</h1>
<div id="form" class="clearfix">
	{{ Form::open(['action' => ['ManageTVShowsController@update', $tvshow->id], 'method' => 'patch']) }}
	{{ Form::label('tvshow_name', 'TV Show Name:', array('class'=>'set-display-block')) }}
	{{ Form::text('tvshow_name', $tvshow->title, array('placeholder'=>'TV Show Name')) }}
	{{ Form::label('start_year', 'Start Year:', array('class'=>'set-display-block')) }}
	{{ Form::text('start_year', $tvshow->show_start_year, array('placeholder'=>'Start Year')) }}
	{{ Form::label('end_year', 'End Year:', array('class'=>'set-display-block')) }}
	{{ Form::text('end_year', $tvshow->show_end_year, array('placeholder'=>'End Year (Leave Blank if None)')) }}
	{{ Form::label('description', 'Description:', array('class'=>'set-display-block')) }}
	{{ Form::textarea('description', $tvshow->description, array('placeholder'=>'Description')) }}
	<br/>
	{{ Form::hidden('cover_upload', asset('static/img/show-covers').'/'.$tvshow->cover) }}
	<p>Cover Preview:</p>
	<img id="cover" src=""/><br/>
	{{ Form::label('total_seasons', 'Total Seasons:', array('class'=>'set-display-block')) }}
	{{ Form::text('total_seasons', $tvshow->seasons, array('placeholder'=>'Total Seasons')) }}
	{{ Form::label('created_by', 'Creator:', array('class'=>'set-display-block')) }}
	{{ Form::text('created_by', $tvshow->created_by, array('placeholder'=>'Created by')) }}
	{{ Form::label('actors', 'Actors:', array('class'=>'set-display-block')) }}
	{{ Form::text('actors', $tvshow->actors, array('placeholder'=>'Actors')) }}
	{{ Form::label('imdb_id', 'IMDB ID:', array('class'=>'set-display-block')) }}
	{{ Form::text('imdb_id', $tvshow->connection->imdb_id, array('placeholder'=>'IMDB ID')) }}
	{{ Form::label('tvdb_id', 'TVDB ID:', array('class'=>'set-display-block')) }}
	{{ Form::text('tvdb_id', $tvshow->connection->tvdb_id, array('placeholder'=>'TVDB ID')) }}
	{{ Form::label('tvrage_id', 'TVRage ID:', array('class'=>'set-display-block')) }}
	{{ Form::text('tvrage_id', $tvshow->connection->tvrage_id, array('placeholder'=>'TVRage ID')) }}
	<label for="ongoing" class="set-display-block">Current Status:</label>
	<select name="ongoing" class="set-display-block">
		<option @if($tvshow->ongoing == 1) selected @endif value='1'>Ongoing</option>
		<option @if($tvshow->ongoing == 0) selected @endif value='0'>Ended</option>
	</select>
	{{ Form::label('current_retrieval_time', "Current Retrieval Time (important auto-saved value, hence disabled):", array('class'=>'set-display-block')) }}
	{{ Form::text('current_retrieval_time', $tvshow->connection->last_retrieval_time, array('class'=>'set-display-block', 'placeholder'=>'Current Retrieval Time', 'disabled' => 'disabled')) }}
	<br/>
	{{ Form::button('Update', array('class'=>'button button-medium button-input-width', 'type'=>'submit')) }}
	{{ Form::close() }}
</div>
@stop

@section('footer-includes')
<script>
//displays the image from the url that is pre-entered into the form field
$("#cover").attr("src", $("input[name=cover_upload]").val());
//if the image url changes then the below method is called
$("input[name=cover_upload]").on('change', function() {
	var cover = $("input[name=cover_upload]").val();
	$("#cover").attr("src", cover);
});
</script>
@stop