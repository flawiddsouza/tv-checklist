@extends('layouts.default')

@section('page-title')
Page not found
@stop

@section('page-content')
	<div class="container container-single">
		<div class="fake-center">
		<h1>404</h1>
		<div class="fill-margin center">This is not the page you are looking for.</div>
		</div>
	</div>
@stop