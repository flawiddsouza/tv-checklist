@extends('admin.default-layout')

@section('extra-title','Manage Users')

@section('section-content')
<h1>Manage Users</h1>

{{ Form::open(['action' => 'ManageUsersController@create', 'method' => 'get']) }}
<button class="button" type="submit"/>Add User</button>
{{ Form::close() }}
<br/>

<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Username</th>
			<th>Email</th>
			<th>Joined on</th>
			<th>Role</th>
		</tr>
	</thead>
	<tbody>
		@foreach($users as $user)
		<tr>
			<td>{{ $user->id }}</td>
			<td><a href="{{ URL::to('/') }}/profile/{{ $user->username }}">{{ $user->username }}</a></td>
			<td>{{ $user->email }}</td>
			<td>{{ $user->created_at->setTimezone($timezone)->format('d-M-Y h:m A') }}</td>
			<td>@foreach($roles as $role) @if($user->role == $role->id) {{ $role->name }} @endif @endforeach</td>
			{{ Form::open(['action' => ['ManageUsersController@edit', $user->id], 'method' => 'get']) }}
			<td><button class='button' type='submit'>Edit</button></td>
			{{ Form::close() }}
			@if($user->id != 1)
			{{ Form::open(['action' => ['ManageUsersController@destroy', $user->id], 'method' => 'delete']) }}
			<td><button class='button' type='submit'>Delete</button></td>
			@endif
		</tr>
		@endforeach
	</tbody>
</table>	
@stop