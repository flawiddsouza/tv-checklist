<?php

class Genres extends Eloquent {

	protected $table = 'genres';

	protected $fillable = 'genres';

}