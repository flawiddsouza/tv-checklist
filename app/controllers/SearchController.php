<?php

class SearchController extends \BaseController {

	public function index()
	{
		$search_term = $_GET['q'];
		$query = $search_term;
		if(!empty($query)) 
		{
			$query = TVShow::search($query)->get();
			if($query->count() == 1)
			{
				$tvshow = TVShow::search($query)->first();
				return Redirect::to('/browse/series/'.$tvshow->id.'/'.str_replace(' ','-', $tvshow->title));
			}
			else
				return View::make('search', compact('search_term', 'query'));
		}
		else
			return Redirect::to('/');
	}

}
