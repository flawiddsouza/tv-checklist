<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTvshowCacheTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tvshow_cache', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tvid');
			$table->integer('total_episodes')->nullable();
			$table->float('avg_rating')->nullable();
			$table->integer('total_user_ratings')->nullable();
			$table->integer('total_watch_count')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tvshow_cache');
	}

}
