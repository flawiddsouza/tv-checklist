@extends('admin.default-layout')

@section('extra-title','Edit Episode')

@section('section-content')
<h1>Edit [{{ $episode->tvshow->title.' - '.$episode->season.'x'.$episode->number.' - '.$episode->title }}]</h1>
<div id="form" class="clearfix">
	{{ Form::open(['action' => ['ManageEpisodesController@update', $episode->id], 'method' => 'patch']) }}
	{{ Form::label('episode_number', 'Episode Number:', array('class'=>'set-display-block')) }}
	{{ Form::text('episode_number', $episode->number, array('placeholder'=>'Episode Number')) }}
	{{ Form::label('episode_name', 'Episode Name:', array('class'=>'set-display-block')) }}
	{{ Form::text('episode_name', $episode->title, array('placeholder'=>'Episode Name')) }}
	{{ Form::label('aired_on', 'Aired On:', array('class'=>'set-display-block')) }}
	{{ Form::text('aired_on', $episode->aired_on, array('placeholder'=>'Aired On')) }}
	{{ Form::label('description', 'Description:', array('class'=>'set-display-block')) }}
	{{ Form::textarea('description', $episode->description, array('placeholder'=>'Description')) }}
	<br/>
	{{ Form::button('Update', array('class'=>'button button-medium button-input-width', 'type'=>'submit')) }}
	{{ Form::close() }}
</div>
@stop