@extends('layouts.default')

@section('page-title')
No Access
@stop

@section('page-content')
	<div class="container container-single">
		<div class="fake-center">
		<h1>401</h1>
		<div class="fill-margin center">Sorry, you're not authorized to access this page.</div>
		</div>
	</div>
@stop