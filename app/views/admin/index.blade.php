@extends('admin.default-layout')

@section('extra-title','Dashboard')

@section('section-content')
<h1>Dashboard</h1>
{{ Form::open(['action' => 'AdminController@doClearCache', 'class' => 'set-display-inline-block']) }}
{{ Form::button('Clear Cache', ['class' => 'button', 'type' => 'submit']) }}
{{ Form::close() }}
{{ Form::open(['action' => 'AdminController@doResetTVCacheCount', 'class' => 'set-display-inline-block']) }}
{{ Form::button('Reset TVCache Count', ['class' => 'button', 'type' => 'submit']) }}
{{ Form::close() }}
@stop