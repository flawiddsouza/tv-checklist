@extends('layouts.default')

@section('page-title')
Reset Password
@stop

@section('page-content')
	@if(Session::has('success'))
		<p class="center alert alert-success">{{ Session::get('success') }}</p>
	@endif

	@if(Session::has('error'))
		<p class="center alert alert-error">{{ Session::get('error') }}</p>
	@endif
	<div class="container container-single center">
		<div id="form">
			{{ Form::open(array('action' => 'RemindersController@postReset', 'method' => 'POST')) }}
				<h1>Reset Password</h1>

				<input type="hidden" name="token" value="{{ $token }}">
				{{ Form::email('email', null, array('class'=>'set-display-block', 'placeholder'=>'Email')) }}
				{{ Form::password('password', array('class'=>'set-display-block', 'placeholder'=>'Password')) }}
				{{ Form::password('password_confirmation', array('class'=>'set-display-block', 'placeholder'=>'Confirm Password')) }}
				{{ Form::button('Reset Password', array('class'=>'button', 'type'=>'submit')) }}

				{{ Form::close() }}
		</div>
	</div>
@stop