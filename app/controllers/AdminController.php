<?php

class AdminController extends BaseController {

	public function index()
	{
		return View::make('admin.index');
	}

	public function doClearCache()
	{
		Cache::flush();
		return Redirect::back()->with('success','Cache cleared!');
	}

	public function doResetTVCacheCount()
	{
		$tvshow = TVShow::all();
		if(!$tvshow->isEmpty()) {
			foreach($tvshow as $this_show) {
				$get_ratings = TVShowRating::where('tvid','=',$this_show->id)->get();
				if(!$get_ratings->isEmpty()) {
					foreach($get_ratings as $get)
						$ratings[] = $get->rating;
				} else {
					$ratings = [0];
				}
				$calculated_average = array_sum($ratings)/count($ratings);

				$tvshowcache = TVShowCache::where('tvid','=',$this_show->id)->First();
				$tvshowcache->avg_rating = round($calculated_average, 1);
				$tvshowcache->total_episodes = Episode::where('tvid','=',$this_show->id)->count();
				$tvshowcache->total_user_ratings = TVShowRating::where('tvid','=',$this_show->id)->count();
				$tvshowcache->total_watch_count = WatchedTVShow::where('tvid','=',$this_show->id)->count();
				$tvshowcache->save();
				return Redirect::back()->with('success','TVCache counters have been re-calculated!');
			}
		}
		else
			return Redirect::back()->with('error','No TV Shows found to be updated into the TVCache table!');
	}

	public function AddShows()
	{
		$series_name = NULL;

		if(!empty($_GET['search'])) {
			$series_name = $_GET['search'];
			$SeriesIDUsingTitle = simplexml_load_file('http://thetvdb.com/api/GetSeries.php?seriesname='.$series_name);
		}

		if(!empty($_GET['seriesid']))
			$seriesid = $_GET['seriesid'];

		if(!empty($seriesid)) {
			$seriesid = $_GET['seriesid'];

			define('API_URL','http://thetvdb.com/api');
			define('API_Key','97389837974409B3');

			$banners_directory = 'http://thetvdb.com/banners/';

			$current_time = simplexml_load_file('http://thetvdb.com/api/Updates.php?type=none');
			$current_time = $current_time->Time;

			$type = 'series';
			$SeriesLang = 'en';

			$GetSeries = API_URL.'/'.API_Key.'/'.$type.'/'.$seriesid.'/all/'.$SeriesLang.'.xml';
			
			//this is checked as to prevent the user from getting redirected to the error page, if the entered series id is invalid
			if (@file_get_contents($GetSeries)): $GetSeriesXML = simplexml_load_file($GetSeries);
			else: return Redirect::action('AdminController@AddShows');
			endif;

			# Variables to be sent to the form
			$tvshow_name = $GetSeriesXML->Series[0]->SeriesName;
			$tvshow_start_year = substr($GetSeriesXML->Series[0]->FirstAired, 0, 4);
			$tvshow_imdb_id = $GetSeriesXML->Series[0]->IMDB_ID;

			// this is the first way to get the total seasons & total episodes
			$total_episodes = 0;
			$total_seasons = 0; //incase total_seasons doesn't come into existense from the below query 
			foreach($GetSeriesXML->Episode as $episode) 
			{
				if(!empty($episode->DVD_season)) 
				{
					for($total_seasons=1; $total_seasons < $episode->DVD_season; $total_seasons++){}
					$total_episodes = $total_episodes+1;
				} 

			}

			$IMDBjson = file_get_contents('http://www.omdbapi.com/?i='.$tvshow_imdb_id.'&plot=short&r=json');

			$IMDBobject = json_decode($IMDBjson);
			$getPoster =  $IMDBobject->Poster;
			$getDescription = $IMDBobject->Plot;
			$getEndYear = substr(str_replace('–', '', $IMDBobject->Year), 4, 7);
			$getCreator = $IMDBobject->Writer;
			$getActors = $IMDBobject->Actors;

			$TVRageURL = 'http://services.tvrage.com/myfeeds/search.php?key=Jha8gNaaY4BeiagDumot&show='.$tvshow_name;
			$TVrageXML = simplexml_load_file($TVRageURL);
			$TVrageID = $TVrageXML->show[0]->showid;

			// this is the second way to get the total seasons, it's executed only if the first way fails
			if($total_seasons == 0)
				$total_seasons = $TVrageXML->show[0]->seasons;

			list($width, $height, $type, $attr) = getimagesize($getPoster);

			// if the height of the cover image retrieved is lesser than 300px,then retrieve one from this place
			if($height < 300) {
				$TVRageURL2 = "http://services.tvrage.com/myfeeds/showinfo.php?key=Jha8gNaaY4BeiagDumot&sid=".$TVrageID;
				if (@file_get_contents($TVRageURL2)) {
					$TVrageXML2 = simplexml_load_file($TVRageURL2);
					$getPoster = $TVrageXML2->image;
				}
			}

			//first way to get the episode listing
			foreach($GetSeriesXML->Episode as $episode) {
				if(!empty($episode->DVD_season)) {
					$episode_season[] = $episode->DVD_season;
					$episode_number[] = $episode->EpisodeNumber;
					$episode_name[] = $episode->EpisodeName;
					$episode_aired_on[] = $episode->FirstAired;
					$episode_description[] = $episode->Overview;
				}
			}

			//another way to get the episode listing from tvdb, if the first way fails
			if(empty($episode_season)) {
				foreach($GetSeriesXML->Episode as $episode) {
					if(!empty($episode->Combined_season)) {
						$total_episodes = $total_episodes+1;
						$episode_season[] = $episode->Combined_season;
						$episode_number[] = $episode->EpisodeNumber;
						$episode_name[] = $episode->EpisodeName;
						$episode_aired_on[] = $episode->FirstAired;
						$episode_description[] = $episode->Overview;
					}
				}
			}

			//using the '?force' in front of the existing url, you force the third way to get the episode listing to be executed
			// do not use '&force'. If you do you will get an undefined offset error
			//also do remember to delete &force from your TVDB ID field in the form, otherwise you'll get an sql error
			if(isset($_GET['force'])) {
				$episode_season = NULL;
			}

			//Third way to get the episode listing if the second way fails
			if(empty($episode_season)) {
				$TVRageURL2 = 'http://services.tvrage.com/myfeeds/episode_list.php?key=Jha8gNaaY4BeiagDumot&sid='.$TVrageID;
				$TVrageXML2 = simplexml_load_file($TVRageURL2);
				foreach ($TVrageXML2->Episodelist->Season as $show)
				{
					foreach($show->attributes() as $season) {}

					foreach($show as $episode)
					{
						$total_episodes = $total_episodes+1;
						$episode_season[] = $season;
						$episode_number[] = $episode->seasonnum;
						$episode_name[] = $episode->title;
						$episode_aired_on[] = $episode->airdate;
						$episode_description[] = " ";
					}
				}
			}
		}
		return View::make('admin.add-shows', compact(
														'series_name',
														'SeriesIDUsingTitle',
														'seriesid',
														'total_episodes',

														'episode_number',
														'episode_name',
														'episode_season',
														'episode_aired_on',
														'episode_description',

														'current_time',
														'tvshow_name',
														'tvshow_start_year',
														'tvshow_imdb_id',

														'getPoster',
														'getDescription',
														'getEndYear',
														'getCreator',
														'getActors',

														'TVrageID',
														'total_seasons'
													));
	}

	public function doAddShows()
	{		
		$tvshow_name_renamed = strtolower(str_replace(' ', '_', Input::get('tvshow_name')));	
		$file_name = $tvshow_name_renamed.'_'.Helper::generateRandomString().'.jpg';
		$img = Image::make(Input::get('cover_upload'))->fit(214, 320)->save(public_path().'/static/img/show-covers/'.$file_name);

		$add_show = new TVShow;
		$add_show->title = Input::get('tvshow_name');
		$add_show->show_start_year = Input::get('start_year');
		if(!empty(Input::get('end_year'))) {
			$add_show->show_end_year = Input::get('end_year');
			$add_show->ongoing = 0;
		}
		else {
			$add_show->show_end_year = " ";
			$add_show->ongoing = 1;
		}
		$add_show->description = Input::get('description');
		$add_show->cover = $file_name;
		$add_show->seasons = Input::get('total_seasons');
		$add_show->created_by = Input::get('created_by');
		$add_show->actors = Input::get('actors');
		$add_show->save();

		// gets the autoincremented value of the field id that was just inserted into the tvshow table, for this tv show
		$tvid = $add_show->id;

		// saves all connections
		$add_show_connection = new TVShowConnection;
		$add_show_connection->tvid = $tvid;
		$add_show_connection->imdb_id = Input::get('imdb_id');
		$add_show_connection->tvdb_id = Input::get('tvdb_id');
		$add_show_connection->tvrage_id = Input::get('tvrage_id');
		$add_show_connection->last_retrieval_time = Input::get('current_retrieval_time');
		$add_show_connection->save();

		// Just here so that it create an entry for this tvshow in its table
		$add_show_cache = new TVShowCache;
		$add_show_cache->tvid = $tvid;
		$add_show_cache->total_episodes = Input::get('total_episodes');
		$add_show_cache->avg_rating = 0.0; // PostgreSQL saves it as 0, whatever you do (even though the field type is float)
		$add_show_cache->total_user_ratings = 0;
		$add_show_cache->total_watch_count = 0;
		$add_show_cache->save();

		//defining all the array variables gotten from the form, for episode entry
		$episode_number = Input::get('episode_number');
		$episode_name = Input::get('episode_name');
		$episode_season = Input::get('episode_season');
		$episode_aired_on = Input::get('episode_aired_on');
		$episode_description = Input::get('episode_description');
		// the variable used for count can be any one of the above defined variables
		for($i=0; $i < count($episode_number); $i++) {
			$add_episode = new Episode;
			$add_episode->tvid = $tvid;
			$add_episode->season = $episode_season[$i];
			$add_episode->title = $episode_name[$i];
			$add_episode->number = $episode_number[$i];
			$add_episode->description = $episode_description[$i];
			$add_episode->aired_on = $episode_aired_on[$i];
			$add_episode->save();
		}

		// Cache has to be cleared for the page to be redirected to the newly added tvshow page, without any errors
		Cache::flush();

		return Redirect::action('BrowseController@individual_tvshow', array('showid' => $tvid))->with('success_add', 'Here it is!');

	}

}