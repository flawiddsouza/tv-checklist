@extends('admin.default-layout')

@section('extra-title','Manage Episodes')

@section('section-content')
<h1>Manage Episodes</h1>

@if(!$tvshow->isEmpty())
{{ Form::open(['action' => 'ManageEpisodesController@create', 'method' => 'get']) }}
<button class="button button-medium" type="submit"/>Add Episode</button>
{{ Form::close() }}
<br/>
@endif

<div class="sort">Show: <a href="?show=ongoing" class="button" />Ongoing</a> <a href="?show=ended" class="button" />Ended</a> <a href="?show=all" class="button" />All</a></div>

@if(!$tvshow->isEmpty())
<span class="select-stuff">
	<div class="set-display-inline-block">
	{{ Form::open(['id' => 'tvshow-selection', 'action' => 'ManageEpisodesController@getTotalSeasons']) }}
	<select name="tvshow">
		@foreach($tvshow as $show)
		<option value="{{ $show->id }}">{{ $show->title }}</option>
		@endforeach
	</select>
	{{ Form::close() }}
	</div>
	<select name="season"></select>

	{{ Form::open(['id' => 'episode-listing', 'action' => 'ManageEpisodesController@getEpisodeListing']) }}
	<input name='tvid' type='hidden' />
	<input name='season' type='hidden' value='1' />
	{{ Form::close() }}
</span>

<br/>

<table>
	<thead>
		<tr>
			<td>E</td>
			<td>Name</td>
			<td>Aired on</td>
		</tr>
	</thead>
	<tbody></tbody>
</table>
@else
No TV Shows exist as of yet for this section
@endif

@stop

@section('footer-includes')
<script>

/*
$("tbody").on('click', 'form#special_delete', function() {
	var c = confirm("Are you sure you want to delete this episode? Know that deleting this episode will also delete any user data associated with it!");
	return c; //you can just return c because it will be true or false
});
*/

// elements that have been added after a document was loaded need to accessed this way
$("tbody").on('click', 'form#delete', function() {
    var c = confirm("Are you sure you want to delete this episode without deleting any user data associated with it?");
	return c; //you can just return c because it will be true or false
});

//to populate the seasons select menu for the pre-selected first tvshow in the tvshow selection list
$( document ).ready(function() {
	$("#tvshow-selection").submit();
});

//to populate the seasons select menu for the selected tvshow in the tvshow selection list
$("#tvshow-selection").on("change", function() {
	$(this).submit();
});

//to perform the populating of the seasons select menu for the selected tvshow in the tvshow selection list
$("#tvshow-selection").submit(function( event ) {
	// Stop form from submitting normally
	event.preventDefault();

	// Get some values from elements on the page:
	var $form = $( this ),
	tvid = $("select[name=tvshow] :selected").val(),
	url = $form.attr( "action" );

	// Send the data using post
	var posting = $.post( url, { id: tvid } );

	// for loop and create the options for the select element with the name'season'
	posting.done(function( data ) {
		var num = parseInt(data);
		var container = $('select[name=season]');
		container.empty();
		for(var i = 1; i <= num; i++) {
			container.append('<option value="'+i+'">'+i+'</option>');
		}
	});

	//submit request for getting episode listing for season 1, on page load for the auto-selected show
	var tvid = $("select[name=tvshow] :selected").val();
	$("input[name=tvid]").attr( "value", tvid );
	$('form#episode-listing').submit();
});

//submit request for episode listing, when the auto selected season is 1
$("select[name=tvshow]").on("change", function() {
	var tvid = $("select[name=tvshow] :selected").val();
	var season = 1;
	$("input[name=tvid]").attr( "value", tvid );
	$("input[name=season]").attr( "value", season );
	$('form#episode-listing').submit();
});

//submit request for episode listing when you select a season manually
$("select[name=season]").on("change", function() {
	var tvid = $("select[name=tvshow] :selected").val();
	var season = $("select[name=season] :selected").val();
	$("input[name=tvid]").attr( "value", tvid );
	$("input[name=season]").attr( "value", season );
	$('form#episode-listing').submit();
});

//get episode listing
$("#episode-listing").submit(function( event ) {
	// Stop form from submitting normally
	event.preventDefault();

	// Get some values from elements on the page:
	var $form = $( this ),
	tvid = $("input[name=tvid]").val(),
	season = $("input[name=season]").val(),
	url = $form.attr( "action" );
	console.log(tvid + season);
	// Send the data using post
	var posting = $.post( url, { tv_id: tvid, tv_season: season } );

	// for loop and create the options for the select element with the name'season'
	posting.done(function( response ) {
		var episodes = $.parseJSON( response );
		console.log( episodes );
		var container = $('tbody');
		container.empty();
		$.each( episodes, function( index, value ){
			//appends have been divided just for the clarity of code and nothing more
			container.append('<tr>');
				container.append('<td>'+value['number']+'</td>');
				container.append('<td>'+value['title']+'</td>');
				container.append('<td>'+value['aired_on']+'</td>');
				container.append('<td><form method="GET" action="{{Request::url()}}/'+value['id']+'/edit" accept-charset="UTF-8"><button class="button" type="submit">Edit</button></form></td>');
				container.append('<td><form id="delete" method="POST" action="{{Request::url()}}/'+value['id']+'" accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><button class="button" type="submit">Delete</button></form></td>');
			container.append('</tr>');
		});
	});
});
</script>
@stop