<?php

class TVShowRating extends Eloquent {

	protected $table = 'tvshow_rating';

	protected $fillable = array('uid', 'tvid', 'rating');
	
}