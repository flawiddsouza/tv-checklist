<!doctype html>
<html>
<head>
	@include('includes.head')
	@yield('extra-head-includes')
</head>
<body>
	<div id="wrapper">
		@include('includes.header')

		<div id="content">
		@yield('page-content')
		</div>

		@include('includes.footer')
	</div>
	@include('includes.google-analytics')
</body>
</html>