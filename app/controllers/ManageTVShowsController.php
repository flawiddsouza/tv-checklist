<?php

class ManageTVShowsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(!isset($_GET['show']))
			$sort_me = NULL;
		else 
			$sort_me = $_GET['show'];

		switch($sort_me) {
			case 'ended': 
			$tvshow = TVShow::with('cache')->where('ongoing','=', 0)->orderBy('id','DESC')->get();
			break;
			case 'ongoing':
			$tvshow = TVShow::with('cache')->where('ongoing','=', 1)->orderBy('id','DESC')->get();
			break;
			case 'all':
			$tvshow = TVShow::with('cache')->orderBy('id','DESC')->get();
			break;
			default:
			$tvshow = TVShow::with('cache')->orderBy('id','DESC')->get();
		}

		return View::make('admin.manage-shows.index', compact('tvshow'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Redirect::action('BrowseController@individual_tvshow', array('showid' => $id));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$tvshow = TVShow::with('connection')->find($id);
		return View::make('admin.manage-shows.edit-show', compact('tvshow'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$tvshow = TVShow::find($id);
		$tvshow->title = Input::get('tvshow_name');
		$tvshow->show_start_year = Input::get('start_year');
		if(!empty(Input::get('end_year'))) 
			$tvshow->show_end_year = Input::get('end_year');
		$tvshow->description = Input::get('description');
		$tvshow->seasons = Input::get('total_seasons');
		$tvshow->created_by = Input::get('created_by');
		$tvshow->actors = Input::get('actors');
		$tvshow->connection->imdb_id = Input::get('imdb_id');
		$tvshow->connection->tvdb_id = Input::get('tvdb_id');
		$tvshow->connection->tvrage_id = Input::get('tvrage_id');
		$tvshow->ongoing = Input::get('ongoing');
		$tvshow->push(); //used to save model + relationships

		Cache::flush(); //clear cache to reflect changes

		return Redirect::action('ManageTVShowsController@index')->with('success', 'The selected show has been successfully updated');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		$tvshow = TVShow::find($id);

		// Delete the tvshow's cover image from the system
		$file_name = $tvshow->cover;
		$destinationPath = public_path().'/static/img/show-covers/';
		File::delete($destinationPath.$file_name);

		// Delete all these
		TVShowCache::where('tvid','=', $id)->First()->delete();
		TVShowConnection::where('tvid','=', $id)->First()->delete();
		Episode::where('tvid','=', $id)->delete();

		// Also delete all the user related data for this tvshow and its episodes
		TVShowRating::where('tvid','=', $id)->delete();
		WatchedTVShow::where('tvid','=', $id)->delete();
		EpisodeRating::where('tvid','=', $id)->delete();
		WatchedEpisode::where('tvid','=', $id)->delete();

		// And finally delete the tvshow itself
		$tvshow->delete();

		Cache::flush(); // clearing cache to remove the deleted tvshow from all cached queries

		return Redirect::back()->with('success', 'The selected show has been removed');
	}


}
