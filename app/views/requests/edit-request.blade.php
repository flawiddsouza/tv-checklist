@extends('layouts.default')

@section('page-title')
Edit Request
@stop

@section('page-content')

	<div id="browse" class="container container-single clearfix">
		<div class="fake-center center">
			<div id="form" class="clearfix">
				<h1>Edit Request</h1>
				@if($errors->any())
				<ul>
					@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
				@endif
				{{ Form::open(['action' => ['RequestsController@update', $request->id], 'method' => 'patch']) }}
				{{ Form::text('tvshow_name', $request->tvshow_name, ['placeholder' => 'Name of the TV Show *', 'class' => 'set-display-block']) }}
				{{ Form::number('first_aired_year', $request->first_aired_year, ['placeholder' => 'First Aired Year *', 'class' => 'set-display-block']) }}
				{{ Form::text('imdb_id', $request->imdb_id, ['placeholder' => 'IMDB ID or Link to the IMDB Page', 'class' => 'set-display-block']) }}
				<p>{{ Form::button('Update Request', ['type' => 'submit', 'class' => 'button button-medium']) }}</p>
				{{ Form::close() }}
			</div>
		</div>
	</div>

@stop