<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodeRatingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('episode_rating', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('uid');
			$table->integer('tvid');
			$table->integer('season');
			$table->integer('episode');
			$table->integer('rating');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('episode_rating');
	}

}
