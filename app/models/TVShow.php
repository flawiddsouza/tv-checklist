<?php

use Nicolaslopezj\Searchable\SearchableTrait;

class TVShow extends Eloquent {

	protected $table = 'tvshow';

	protected $fillable = array('title', 'show_start_year', 'show_start_year', 'description', 'cover', 'seasons', 'created_by', 'actors');

	//Used by the 'Browse' page and individual show pages
	public function cache()
	{
		return $this->hasOne('TVShowCache', 'tvid');
	}

	//Used for Administrative tasks like adding and updating tvshows and episode listings
	public function connection()
	{
		return $this->hasOne('TVShowConnection', 'tvid');
	}

	//Implementing Genres
	public function tvshow_genres()
	{
		return $this->hasMany('TVShowGenre','tvid');
	}

	//Watched Shows
	public function watched_episode()
	{
		return $this->hasMany('WatchedEpisode','tvid');
	}

	use SearchableTrait;
	
	protected $searchable = [
		'columns' => [
			'title' => 30,
		],
	];

}