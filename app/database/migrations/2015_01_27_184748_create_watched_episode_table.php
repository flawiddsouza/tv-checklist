<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWatchedEpisodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('watched_episode', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('uid');
			$table->integer('tvid');
			$table->integer('episode_id');
			$table->integer('season');
			$table->integer('episode');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('watched_episode');
	}

}
