@extends('layouts.default')

@section('page-title')
Watched Shows
@stop

@section('page-content')

	<div id="browse" class="container container-single clearfix">
		<div class="fake-center">
		<h1>Watched Shows</h1>
		<p class="margin">If you've watched one or more episodes of a tv show, it will be listed here for you to see.</p>
				@foreach($watched_tvshow as $show)
				<div class="mini-container">
					<div><p title="{{ $show->title }}" class="ellipsis"><strong>{{ $show->title }}</strong></p></div>
					<a href="/browse/series/{{ $show->id }}/{{ str_replace(' ','-', $show->title) }}">{{ HTML::image($covers_directory.'/'.$show->cover, $show->title) }}</a>
					<div>Watched <strong>{{ WatchedEpisode::where('tvid','=',$show->id)->where('uid','=',Auth::id())->count() }}</strong> episodes</div>
				</div>
			@endforeach
		</div>
		@if(empty($show->id))
			<div class="fill-margin center">You don't seem to have watched any episodes.</div>
		@endif
	</div>

@stop