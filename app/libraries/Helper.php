<?php
	
class Helper {

	public static function generateRandomString($length = 10) 
	{
		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	}

	public static function checkNotEmpty()
	{
		foreach(func_get_args() as $arg)
			if(!empty($arg))
				continue;
			else
				return false;
			return true;
	}

}