@extends('admin.default-layout')

@section('extra-title','Manage Roles')

@section('section-content')
<h1>Manage Roles</h1>

{{ Form::open(['action' => 'RolesController@create', 'method' => 'get']) }}
<button class="button" type="submit"/>Add Role</button>
{{ Form::close() }}
<br/>

<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Role Name</th>
		</tr>
	</thead>
	<tbody>
		@foreach($roles as $role)
		<tr>
			<td>{{ $role->id }}</td>
			<td>{{ $role->name }}</td>
			{{ Form::open(['action' => ['RolesController@edit', $role->id], 'method' => 'get']) }}
			<td><button class='button' type='submit'>Edit</button></td>
			{{ Form::close() }}
			@if($role->id != 1 && $role->id != 2)
			{{ Form::open(['action' => ['RolesController@destroy', $role->id], 'method' => 'delete']) }}
			<td><button class='button' type='submit'>Delete</button></td>
			{{ Form::close() }}
			@endif
		</tr>
		@endforeach
	</tbody>
</table>	
@stop