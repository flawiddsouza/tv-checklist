<?php

class RolesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$roles = Role::orderBy('id')->get();
		return View::make('admin.manage-roles.index', compact('roles'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.manage-roles.add-role');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$new_role_name = Input::get('role_name');
		if(!empty($new_role_name)) 
		{
			Role::create(['name' =>  $new_role_name]);
			return Redirect::action('RolesController@index')->withSuccess('A new role has been added successfully!');
		} 
		else
			return Redirect::back()->withError('An Empty form was submitted! Please try again.');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$role = Role::find($id);
		return View::make('admin.manage-roles.edit-role', compact('role'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$updated_role_name = Input::get('role_name');
		if(!empty($updated_role_name)) 
		{
			Role::find($id)->update(['name' =>  $updated_role_name]);
			return Redirect::action('RolesController@index')->withSuccess('The selected role has been updated!');
		} 
		else
			return Redirect::back()->withError('An Empty form was submitted! Please try again.');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Role::find($id)->delete();
		return Redirect::back()->withSuccess('The selected role has been deleted!');
	}


}
