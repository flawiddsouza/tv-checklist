<?php

class BrowseController extends BaseController {

	public function showBrowse()
	{
		// all latest('updated_at') does is orderby latest updated first and oldest updated last
		$shows = TVShow::with('cache')->latest('updated_at')->rememberForever()->get();

		if (Auth::check()) {
		$watched = Auth::user()->shows;
		} else {
			// just create an empty collection so we can assume a consistent API
			$watched = new \Illuminate\Support\Collection;
		}

		return View::make('browse', compact('shows', 'watched'), ['covers_directory' => "static/img/show-covers"]);
	}

	public function doBrowse()
	{
		if(Input::except('_token')){
			$input = Input::except('_token');
			foreach ($input as $showid) {
				$watchedshow = new WatchedTVShow;
				$watchedshow->uid = Auth::id();
				$watchedshow->tvid = $showid;
				$watchedshow->save();
			}

			if(count($input)==1) {
				$message = '1 show was added to your '.HTML::linkAction('UserController@showMyShows','list');
			} elseif((count($input)!=1)) {
				$message = ''.count($input).' shows were added to your '.HTML::linkAction('UserController@showMyShows','list');
			}
			return Redirect::back()->with('success', $message);
		} else {
			return Redirect::back()->with('error', 'No shows were selected to be added!');
		}
	}



	public function individual_tvshow($showid)
	{	if(!empty(TVShow::find($showid))) {
			$add = 1;
			$rating = 0;
			$watched_season = [];
			$rated_season = [];

			if(Auth::check()) {
				$uid = Auth::id();

				$myshow = WatchedTVShow::where('uid', $uid)->where('tvid', $showid)->get();
				foreach($myshow as $show){
					if($showid == $show->tvid)
					$add = 0;
				}

				$show_rating = TVShowRating::where('uid', $uid)->where('tvid', $showid)->get();
				foreach($show_rating as $user_tv_rating){
					$rating = $user_tv_rating->rating;
				}

			}

			$tvshow = TVShow::with('cache')->rememberForever()->find($showid);
			for($i=1; $i<=$tvshow->seasons; $i++) {
				$episode[$i] = Episode::where('tvid', $showid)->where('season', $i)->orderBy('number')->rememberForever()->get();
				$ep_count[$i] = Episode::where('tvid', $showid)->where('season', $i)->rememberForever()->count();
				if(Auth::check()) {
					$watched_season[$i] = WatchedEpisode::where('uid', $uid)->where('tvid', '=', $showid)->where('season', $i)->get();
					$rated_season[$i] = EpisodeRating::where('uid', $uid)->where('tvid', $showid)->where('season', $i)->get();
				}
			}

			return View::make('tvshow', [
				'tvshow' 			=> $tvshow,
				'covers_directory'	=> "static/img/show-covers",
				'episode' 			=> $episode,
				'add'				=> $add,
				'rating'			=> $rating,
				'watched_season'	=> $watched_season,
				'rated_season'		=> $rated_season,
				'ep_count'			=> $ep_count
				]);
		} else 
		{
			//redirects to the Browse page, if the slug showid doesn't match any tvshow in the tvshow table
			return Redirect::action('BrowseController@showBrowse'); 
		}
	}


	public function add_watchedshow($showid)
	{	
		$this_show = new WatchedTVShow;
		$this_show->uid = Auth::id();
		$this_show->tvid = $showid;
		$this_show->save();
		$message = 'This show has been added to your '.HTML::linkAction('UserController@showMyShows','list');
		return Redirect::back()->with('success_add', $message);
	}

	public function remove_watchedshow($showid)
	{
		$uid = Auth::id();
		$myshow = WatchedTVShow::where('uid', $uid)->where('tvid', $showid)->get();
		foreach($myshow as $show){
			$this_show = WatchedTVShow::find($show->id);
			$this_show->delete();
		}
		$message = 'This show has been removed from your '.HTML::linkAction('UserController@showMyShows','list');
		return Redirect::back()->with('success_remove',  $message);
	}


	public function add_show_rating($showid)
	{
		$uid = Auth::id();
		$input = Input::except('_token');
		$show_rating = TVShowRating::where('uid', $uid)->where('tvid', $showid)->get();

		if($show_rating->isEmpty()) 
		{
			foreach ($input as $rating) {
				$addshowrating = new TVShowRating;
				$addshowrating->uid = Auth::id();
				$addshowrating->tvid = $showid;
				$addshowrating->rating = $rating;
				$addshowrating->save();
			}

			// begin updating TVShowCache table for this tvshow
			$get_ratings = TVShowRating::where('tvid', $showid)->get();
			if(!$get_ratings->isEmpty())
			{
				foreach($get_ratings as $get)
					$ratings[] = $get->rating;
			} 
			else 
			{
				$ratings = [0];
			}
			$calculated_average = array_sum($ratings)/count($ratings);

			$update_tvshowcache = TVShowCache::where('tvid', $showid)->First();
			$update_tvshowcache->avg_rating = round($calculated_average, 1);
			$update_tvshowcache->total_user_ratings = $update_tvshowcache->total_user_ratings+1;
			$update_tvshowcache->save();
			// end of code block for updating TVShowCache table for this tvshow
		} 
		else 
		{
			foreach($show_rating as $get_rating) {
				foreach ($input as $rating) {
				$addshowrating = TVShowRating::find($get_rating->id);
				$addshowrating->rating = $rating;
				$addshowrating->save();
				}
			}

			// begin updating TVShowCache table for this tvshow
			$get_ratings = TVShowRating::where('tvid','=', $showid)->get();
			if(!$get_ratings->isEmpty())
			{
				foreach($get_ratings as $get)
					$ratings[] = $get->rating;
			} 
			else 
			{
				$ratings = [0];
			}
			$calculated_average = array_sum($ratings)/count($ratings);

			$update_tvshowcache = TVShowCache::where('tvid', $showid)->First();
			$update_tvshowcache->avg_rating = round($calculated_average, 1);
			$update_tvshowcache->save();
			// end of code block for updating TVShowCache table for this tvshow
		}
	}


	public function add_watched_episode($showid, $season) {
		if (Request::ajax())
		{
			$uid = Auth::id();

			//adding and removing watched episodes
			$input = Input::get('formData');
			$input_ep_id = Input::get('EpIdArray');

			$i=1;	
			foreach($input as $episode) {
				if($episode != 0){
					$watched_episode = WatchedEpisode::where('uid', $uid)->where('tvid', $showid)->where('season', $season)->where('episode', $i)->get();
					if(!empty($watched_episode)) {
						foreach($watched_episode as $this_episode) {
							$remove_watched_episode = WatchedEpisode::find($this_episode->id);
							$remove_watched_episode->delete();
						}
					}
					$add_watched_episode = new WatchedEpisode;
					$add_watched_episode->uid = Auth::id();
					$add_watched_episode->tvid = $showid;
					$add_watched_episode->episode_id = $input_ep_id[$i-1];
					$add_watched_episode->season = $season;
					$add_watched_episode->episode = $i;
					$add_watched_episode->save();
				} elseif($episode == 0) {
					$watched_episode = WatchedEpisode::where('uid', $uid)->where('tvid', $showid)->where('season', $season)->where('episode', $i)->get();
					if(!empty($watched_episode)) {
						foreach($watched_episode as $this_episode) {
							$remove_watched_episode = WatchedEpisode::find($this_episode->id);
							$remove_watched_episode->delete();
						}
					}
				}
				$i++;
			}

			//adding and removing ratings for episodes
			$input2 = Input::get('Rating');
			$i=1;
			foreach($input2 as $rating) {
				if($rating != 0){
					$episode_rating = EpisodeRating::where('uid', $uid)->where('tvid', $showid)->where('season', $season)->where('episode', $i)->get();
					if(!empty($episode_rating)) {
						foreach($episode_rating as $this_episode) {
							$remove_episode_rating = EpisodeRating::find($this_episode->id);
							$remove_episode_rating->delete();
						}
					}
					$add_episode_rating = new EpisodeRating;
					$add_episode_rating->uid = Auth::id();
					$add_episode_rating->tvid = $showid;
					$add_episode_rating->season = $season;
					$add_episode_rating->episode = $i;
					$add_episode_rating->rating = $rating;
					$add_episode_rating->save();
				} elseif($rating == 0) {
					$episode_rating = EpisodeRating::where('uid', $uid)->where('tvid', $showid)->where('season', $season)->where('episode', $i)->get();
					if(!empty($episode_rating)) {
						foreach($episode_rating as $this_episode) {
							$remove_episode_rating = EpisodeRating::find($this_episode->id);
							$remove_episode_rating->delete();
						}
					}
				}
				$i++;
			}
		} 
		else
			return Redirect::back()->withSuccessRemove("You don't have javascript enabled, so you won't be able to mark any episodes as watched or rate them!");
	}

	public function getEpisodeDescription($tvid, $season, $epnum)
	{
		try {
			$description = Episode::where('tvid', $tvid)->where('season', $season)->where('number', $epnum)->select('description')->FirstorFail()->description;
		}

		catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			return "No Description Available for this episode";
		}
		if($description != "" && $description != " ")
			return $description;
		else
			return "No Description Available for this episode";
	}

}