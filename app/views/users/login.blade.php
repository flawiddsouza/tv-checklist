@extends('layouts.default')

@section('page-title')
Login
@stop

@section('page-content')
	@if(Session::has('success'))
		<p class="center alert alert-success">{{ Session::get('success') }}</p>
	@endif
	@if(Session::has('logout'))
		<p class="center alert">{{ Session::get('logout') }}</p>
	@endif
	@if(Session::has('error'))
		<p class="center alert alert-error">{{ Session::get('error') }}</p>
	@endif
	<div class="container container-single center">
		<div id="form">
			{{ Form::open() }}
				<h1>Login</h1>

				<input name="username" type="text" required placeholder="Username" class="set-display-block" value="" />

				<input name="password" type="password" required placeholder="Password" value="" />

				<div class="set-display-block">
				{{ Form::label('remember_me','Remember Me') }}
				{{ Form::checkbox('remember_me') }}
				
				{{ Form::button('Login', array('class'=>'button', 'type'=>'submit')) }}</div>
			{{ Form::close() }}
			{{ HTML::linkAction('RemindersController@getRemind','Forgot Password?') }}
		</div>
	</div>
@stop