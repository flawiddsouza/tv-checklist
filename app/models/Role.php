<?php

class Role extends Eloquent {

    protected $table = 'role';
    
    protected $fillable = ['name'];
}