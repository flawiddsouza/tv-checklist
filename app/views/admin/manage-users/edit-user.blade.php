@extends('admin.default-layout')

@section('extra-title','Edit User')

@section('section-content')
<h1>Edit User</h1>
<div id="form" class="clearfix">
	{{ Form::open(array('action' => ['ManageUsersController@update', $user->id], 'method' => 'patch')) }}
	<label for="username" class="set-display-block">Username:</label>
	<input placeholder="Username" class="set-display-block" name="username" type="text" required value="{{ $user->username }}" />
	<label for="password" class="set-display-block">Password:</label>
	<input placeholder="Password" class="set-display-block" name="password" type="text" required value="" />
	<label for="email" class="set-display-block">Email Address:</label>
	<input placeholder="Email Address" class="set-display-block" name="email" type="text" required value="{{ $user->email }}" />
	<label for="role" class="set-display-block">Role:</label>
	<select name="role">
		@foreach($roles as $role)
		<option @if($user->role == $role->id) selected @endif value="{{ $role->id }}">{{ $role->name }}</option>
		@endforeach
	</select>
	<br/><br/>
	{{ Form::button('Update User', array('class'=>'button button-medium button-input-width', 'type'=>'submit')) }}
	{{ Form::close() }}
</div>
@stop