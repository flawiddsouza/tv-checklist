	<div id="show-data-container" class="container clearfix">
		@if($add==1)
		{{ Form::open(array('action' => ['BrowseController@add_watchedshow', $tvshow->id])) }}
		<div class="add-show">{{ Form::button('Add to My Shows', array('class'=>'button button-medium', 'type'=>'submit')) }}</div>
		@elseif($add==0)
		{{ Form::open(array('action' => ['BrowseController@remove_watchedshow', $tvshow->id],'method'=>'delete')) }}
		<div class="remove-show">{{ Form::button('Remove from My Shows', array('class'=>'button button-medium', 'type'=>'submit')) }}</div>
		@endif
		{{ Form::close() }}
		<img id="show-cover" src="{{ asset($covers_directory).'/'.$tvshow->cover }}" alt="{{ $tvshow->title }}" />
		<article itemprop="itemReviewed" itemscope itemtype="http://schema.org/TVSeries">
			<div id="series-title"><h2 itemprop="name">{{ $tvshow->title }} ({{ $tvshow->show_start_year }}–{{ $tvshow->show_end_year }})</h2></div>
			<div id="tv-rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
				<h4>Rated <span class="highlight-golden" itemprop="ratingValue">{{ $tvshow->cache->avg_rating }}</span> by <span itemprop="ratingCount">{{ $tvshow->cache->total_user_ratings }}</span>@if($tvshow->cache->total_user_ratings == 1) user @else users @endif</h4>
				<meta itemprop="bestRating" content="10"/>
      			<meta itemprop="worstRating" content="1"/>
				{{ Form::open(array('id'=> 'show_rating', 'action' => ['BrowseController@add_show_rating', $tvshow->id])) }}
					<div id="your-rating">
						<strong>Your Rating:</strong> 
						<span class='star-rating'>
						@for($i=1; $i<=10; $i++)
							<input type="radio" title="Click to rate: {{$i}}" @if($i==$rating) checked @endif name="rate" value='{{ $i }}'><i></i>
						@endfor
						</span>
						<span class="highlight-lime-green" id='your-rating'>@if($rating!=0)<span id='my-rating'>{{$rating}}</span>@else<span id="before-rating" class="dark-grey">-</span>@endif<span class="dark-grey">/10</span></span>
					</div>
				{{ Form::close() }}
			</div>
			<p id="show-description">{{ $tvshow->description }}</p>
			<div id="show-details">
				@if($tvshow->created_by !="N/A")
				<p><strong>Created by:</strong> {{ $tvshow->created_by }}</p>
				@else
				<p></p>
				@endif
				<p><strong>Actors:</strong> {{ $tvshow->actors }}</p>
			</div>
			<div id="watched-by"><p>Watched by <span class="highlight-orange">{{ $tvshow->cache->total_watch_count }}</span>@if($tvshow->cache->total_watch_count == 1) user @else users @endif</p></div>
		</article>
	</div>