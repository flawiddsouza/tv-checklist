@extends('admin.default-layout')

@section('extra-title','Add Episode')

@section('section-content')
<h1>Add Episode to [ <span id="title-with-season"/></span> ]</h1>

<div class="sort">Show: <a href="?show=ongoing" class="button" />Ongoing</a> <a href="?show=ended" class="button" />Ended</a> <a href="?show=all" class="button" />All</a></div>

@if(!$tvshow->isEmpty())
<span class="select-stuff">
	<div class="set-display-inline-block">
	{{ Form::open(['id' => 'tvshow-selection', 'action' => 'ManageEpisodesController@getTotalSeasons']) }}
	<select name="tvshow">
		@foreach($tvshow as $show)
		<option value="{{ $show->id }}">{{ $show->title }}</option>
		@endforeach
	</select>
	{{ Form::close() }}
	</div>
	<select name="season"></select>
</span>
@endif

<div id="form" class="clearfix">
	{{ Form::open(['action' => ['ManageEpisodesController@store']]) }}
	<input type="hidden" name="tvid">
	<input type="hidden" name="season">
	<label for="episode_number" class="set-display-block">Episode Number:</label>
	<input placeholder="Episode Number" name="episode_number" type="text" required value="">
	<label for="episode_name" class="set-display-block">Episode Name:</label>
	<input placeholder="Episode Name" name="episode_name" type="text" required value="">
	<label for="aired_on" class="set-display-block">Aired On:</label>
	<input placeholder="Aired On" name="aired_on" type="date" required value="">
	<label for="description" class="set-display-block">Description:</label>
	<textarea placeholder="Description" name="description" cols="50" rows="10" required></textarea>
	<br/>
	<button class="button button-medium button-input-width" type="submit">Add Episode</button>
	{{ Form::close() }}
</div>
@stop

@section('footer-includes')
<script>

//to populate the seasons select menu for the pre-selected first tvshow in the tvshow selection list
$( document ).ready(function() {
	$("#tvshow-selection").submit();
});

//to populate the seasons select menu for the selected tvshow in the tvshow selection list
$("#tvshow-selection").on("change", function() {
	$(this).submit();
});

//to perform the populating of the seasons select menu for the selected tvshow in the tvshow selection list
$("#tvshow-selection").submit(function( event ) {
	// Stop form from submitting normally
	event.preventDefault();

	// Get some values from elements on the page:
	var $form = $( this ),
	tvid = $("select[name=tvshow] :selected").val(),
	url = $form.attr( "action" );

	// Send the data using post
	var posting = $.post( url, { id: tvid } );

	// for loop and create the options for the select element with the name'season'
	posting.done(function( data ) {
		var num = parseInt(data);
		var container = $('select[name=season]');
		container.empty();
		for(var i = 1; i <= num; i++) {
			container.append('<option value="'+i+'">'+i+'</option>');
		}
	});
});

// make title for pre-selected tvshow and season
$( document ).ready(function() {
	var tvshow = $('select[name=tvshow] :selected').text();
	var season = 1;
	$('span#title-with-season').html(tvshow +' - Season '+ season);

	// create hidden input fields for the pre-selected tvshow and season
	var tvid = $('select[name=tvshow] :selected').val();
	$('input[name=tvid]').replaceWith('<input name="tvid" type="hidden" value="'+tvid+'">')
	$('input[name=season]').replaceWith('<input name="season" type="hidden" value="'+season+'">')
});

// make title for pre-selected tvshow and but manually select season
$("select[name=season]").on("change", function() {
	var tvshow = $('select[name=tvshow] :selected').text();
	var season = $("select[name=season] :selected").val();
	$('span#title-with-season').html(tvshow +' - Season '+ season);

	// create hidden input fields for pre-selected tvshow and but manually select season
	var tvid = $('select[name=tvshow] :selected').val();
	$('input[name=tvid]').replaceWith('<input name="tvid" type="hidden" value="'+tvid+'">')
	$('input[name=season]').replaceWith('<input name="season" type="hidden" value="'+season+'">')
});

// make title for manually selected tvshow and pre-selected+manually selected season
$("select[name=tvshow]").on("change", function() {
	var tvshow = $('select[name=tvshow] :selected').text();
	var season = 1;
	$('span#title-with-season').html(tvshow +' - Season '+ season);

	// create hidden input fields for manually selected tvshow and pre-selected+manually selected season
	var tvid = $('select[name=tvshow] :selected').val();
	$('input[name=tvid]').replaceWith('<input name="tvid" type="hidden" value="'+tvid+'">')
	$('input[name=season]').replaceWith('<input name="season" type="hidden" value="'+season+'">')
});


</script>
@stop