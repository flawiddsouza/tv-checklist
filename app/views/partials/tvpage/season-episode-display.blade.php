	<div id="tv-seasons-container" class="container clearfix">
		<div id="loading"></div>
		<div id="usual1" class="usual"> 
			<ul> 
				Season 
				@for($season = 1; $season <= $tvshow->seasons; $season++)
				<li><a @if($season == $tvshow->seasons) class="selected" @endif href="#{{ $season }}">{{ $season }}</a></li> 
				@endfor
			</ul>
			<div id="fake-box">
				@for($season = 1; $season <= $tvshow->seasons; $season++)
				<div id="{{ $season }}">
					{{ Form::open(array('id'=> 'add_watched_episode'.$season, 'action' => ['BrowseController@add_watched_episode', $tvshow->id, $season])) }}
				  	<table>
				  		<thead>
							<tr>
								<th>#</th>
								<th>Episodes</th>
								<th>Premiered On</th>
								<th>Rating
									<a class="tooltip">
									<strong> ?</strong>
									<span>
										<img class="callout" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAWCAYAAAD0OH0aAAAAb0lEQVQ4T63SwRGAIAxEUSjIFiyGgqzIKmgIJ4c4AiG7ceD8HzkkOQVeva+W2V7i4yyJAhrL5xB8YwjG2AVWvASr2ARePAEUd4CJX8DG/4FIdkq3aQZNp4GQeUuhxem5h07DQ3vP25oEJ4yIBrrcBwdrVG2hI7DlAAAAAElFTkSuQmCC" alt=""/>
										You need to hit the Save button after rating the episodes you like, otherwise the ratings will not be saved!
									</span>
									</a>
								</th>
								<th>{{ Form::button('Save', array('class'=>'button button-tiny', 'type'=>'submit')) }}</th>
							</tr>
						</thead>
						<tbody>
							<tr><td colspan=4></td><td><input data-season="{{ $season }}" class="selectall" type="checkbox">Watched</td></tr>
							@foreach($episode[$season] as $episode_data)
							<tr>
								<td>{{ $episode_data->number }}</td>
								<td>
									<p class="ellipsis" title="{{{ $episode_data->title }}}">
										<a href="#" class="get-desc" data-tvid="{{ $tvshow->id }}" data-season="{{ $season }}" data-episode="{{ $episode_data->number }}">{{ $episode_data->title }}</a>
										<div class="desc{{ $season }}{{ $episode_data->number }} desc-clear"></div>
									</p>
								</td>
								<td>{{ date("d M. Y", strtotime($episode_data->aired_on)) }}</td>
								<td>
								<span class='star-rating'>
								@for($i=1; $i<=10; $i++)
								<input type="radio" title="Click to rate: {{$i}}" @if(Auth::check()) @foreach($rated_season[$season] as $this_thing) @if($this_thing->episode == $episode_data->number) @if($this_thing->rating == $i) checked @endif @endif @endforeach @endif name="rate{{$season.$episode_data->number}}" value='{{ $i }}'><i></i>
								@endfor
								</span>
								<span class="remove-episode-rating"><a href="#" data-season="{{ $season }}" data-episode="{{ $episode_data->number }}"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAABIElEQVQ4T63USyuFQRgH8N9ZWVhQsvDpXDaIJEIUUYRILks7l09mKcXO2UiP3tGcc96rzHLemV//eed5puefRy/zxtD/oz+Ft9ibwHG8YgkPHdED7GESn3nCZdxhHo8t0SPsYxH3ecK0P6FzeGpAj4tkv1gZGHOB3iLQ5wr0FDt5srQuP3K+tw69wGYZVpUwP/4NpvFRTF5hvQprAuN7YHH7Ma6xVoe1AVPauP2VJqwteI4tfGGhqU6rLiUlO8FukSw6KY5dW6d14EjRYrVAK+u0Cox2Oqz4Z7VoGRh9GV0w0AFDBZ7Q2eHiHwa3cdbmNrPjD6A5uIHLllgKPJI0gTN4Kfr459XoMAKNdoznq58njIn3DlC+dCK1Z1Mddva/AXwYPxVNOcV5AAAAAElFTkSuQmCC" alt=""></a></span>
								</td>
								<td><input class="checkbox{{ $season }}" @if(Auth::check()) @foreach($watched_season[$season] as $this_episode) @if($this_episode->episode == $episode_data->number) checked @endif @endforeach @endif value="{{ $episode_data->id }}" type="checkbox"></td>
							</tr>
							@endforeach
						</tbody>
				  	</table>
					{{ Form::close() }}
				</div> 
				@endfor
			</div>
		</div> 
	</div>
	</div>