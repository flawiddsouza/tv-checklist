@extends('layouts.default')

@section('page-title')
Register
@stop

@section('extra-head-includes')
	<script src="{{asset('static/js/jstz.min.js')}}"></script>
@stop

@section('page-content')
	@if(Session::has('success'))
		<p class="center alert alert-success">{{ Session::get('success') }}</p>
	@endif
	@if(Session::has('error'))
		<p class="center alert alert-error">{{ Session::get('error') }}</p>
	@endif
	<div class="container container-single center">
		<div id="form">
			{{ Form::open() }}
			<h1>Register</h1>
			@if($errors->any())
		 	<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
		    </ul>
		 	@endif

			<input class="set-display-block" placeholder="Username" name="username" type="text" required value="" />
			<input class="set-display-block" placeholder="Email Address" name="email" type="email" required value="" />
			<input class="set-display-block" placeholder="Password" name="password" type="password" required value="">
			<input class="set-display-block" placeholder="Confirm Password" name="password_confirmation" type="password" required value="">
			<input name="timezone" type="hidden" value="">

			<p>{{ Form::button('Create Account', array('class'=>'button', 'type'=>'submit')) }}</p>
			{{ Form::close() }}
		</div>
	</div>

	<script>
		var timezone = jstz.determine();
		$( 'input[name=timezone]' ).attr('value', timezone.name());
	</script>

@stop