<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('episode', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tvid');
			$table->integer('season');
			$table->string('title');
			$table->integer('number');
			$table->text('description');
			$table->date('aired_on');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('episode');
	}

}
