<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>{{ $user_profile->username }} - TV Checklist</title>
	<link rel="stylesheet" href="{{asset('static/css/embed.css')}}">
</head>
<body>
	<div class="container block clearfix">
		<div class="centered">
			<img class="profile-image" src="https://s3.amazonaws.com/uifaces/faces/twitter/tofslie/73.jpg"/>
			<h4><a href="{{ URL::to('/') }}/profile/{{ $user_profile->username }}">{{ $user_profile->username }}</a></h4>
			<p>Has watched a total of <span class="highlight-golden">{{ $total_watched_shows }}</span> @if($total_watched_shows == 1) TV Show @else TV Shows @endif and <span class="highlight-orange">{{ $total_watched_episodes }}</span> @if($total_watched_episodes == 1) Episode. @else Episodes. @endif</p>
		</div>
	</div>
</body>
</html>