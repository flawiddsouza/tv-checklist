@extends('layouts.default')

@section('page-title')
Settings
@stop

@section('page-content')
	@if(Session::has('success'))
		<p class="center alert alert-success">{{ Session::get('success') }}</p>
	@endif
	@if(Session::has('error'))
		<p class="center alert alert-error">{{ Session::get('error') }}</p>
	@endif
	<div class="container container-single">
		<div class="fake-center">
			<h1>Settings</h1>
			<div id="form">
				{{ Form::open(array('action' => 'UserController@putPassword', 'method' => 'patch')) }}
				<br/>
				<label>Change Password:</label>
				<br/>
				<input name="current_password" type="password" required placeholder="Current Password"/>
				<br/>
				<input name="new_password" type="password" required placeholder="New Password"/>

				<p>{{ Form::button('Change Password', array('class'=>'button', 'type'=>'submit')) }}</p>
				{{ Form::close() }}
				

				{{ Form::open(array('action' => 'UserController@putEmailAddress', 'method' => 'patch')) }}
				<br/>
				<label>Change Email Address:</label>
				<br/>
				<input name="email" type="email" required placeholder="Email Address" value='{{ Auth::user()->email }}' />
				<p>{{ Form::button('Change Email Address', array('class'=>'button', 'type'=>'submit')) }}</p>
				{{ Form::close() }}


				{{ Form::open(array('action' => 'UserController@putName', 'method' => 'put')) }}
				<br/>
				<label>Change Name:</label>
				<br/>
				<input name="name" type="text" required placeholder="Name" value='{{ Auth::user()->name }}' />
				<p>{{ Form::button('Change Name', array('class'=>'button', 'type'=>'submit')) }}</p>
				{{ Form::close() }}				
			</div>
		</div>
	</div>
@stop