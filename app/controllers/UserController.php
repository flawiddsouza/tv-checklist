<?php

class UserController extends BaseController {

	public function showRegister()
	{
		return View::make('users.register');
	}

	public function doRegister()
	{
			$validator = Validator::make(Input::all(), User::$rules);
			 
			if ($validator->passes()) {
				$user = new User;
				$user->username = Input::get('username');
				$user->email = Input::get('email');
				$user->password = Hash::make(Input::get('password'));
				$user->role = 2;

				if(Input::get('timezone') != "")
					$user->timezone = Input::get('timezone');
				else
					$user->timezone = NULL;

				$request = Request::instance();
				$request->setTrustedProxies(['128.199.204.21']);
				$user->ip = $request->getClientIp();
				
				$user->save();
				//To make the first user, the admin user
			if(User::count() == 1) {
				$make_admin = User::find(1);
				$make_admin->role = 1;
				$make_admin->save();
			}
				return Redirect::to('login')->with('success', 'Thanks for registering!');
			} else {
				return Redirect::to('register')->with('error', 'The following errors occurred')->withErrors($validator)->withInput();
			}
	}

	public function showLogin()
	{
		return View::make('users.login');
	}

	public function doLogin()
	{
		//check if the entered username is an email or an  username and login, and set the login field accordingly
		$field = filter_var(Input::get('username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username'; 
		if (Auth::attempt(array($field => Input::get('username'), 'password' => Input::get('password')), Input::has('remember_me'))) {
			return Redirect::to('myshows')->with('success', 'You are now logged in!');
		} else {
			return Redirect::to('login')
				->with('error', 'Your username/password combination was incorrect')
				->withInput();
		}
	}

	public function doLogout()
	{
		Auth::logout();
		return Redirect::to('/')->with('logout', 'You are now logged out!');
	}

	public function showMyShows()
	{
		$watched_tvshow = WatchedTVShow::where('uid', '=', Auth::id())->with('tvshow')->orderBy('id', 'desc')->get();
		$covers_directory = "static/img/show-covers";
		return View::make('users.myshows', compact('watched_tvshow', 'covers_directory'));
	}

	public function doMyShows()
	{
		if(Input::except('_token')){
			$input = Input::except('_token');
			foreach ($input as $id) {
				$this_show = WatchedTVShow::find($id);
				$this_show->delete();
			}

			if(count($input)==1) {
				$message = '1 show was removed from your list';
			} else {
				$message = ''.count($input).' shows were removed from your list!';
			}
			return Redirect::back()->with('success', $message);
		} else {
			return Redirect::back()->with('error', 'No shows were selected to be removed!');
		}
	}

	public function WatchedShows()
	{
		$watched_tvshow = TVShow::whereHas('watched_episode', function($query)
		{
			$query->where('uid','=', Auth::id())->where('tvid','>', 0);
		})->get();
		$covers_directory = "static/img/show-covers";
		return View::make('users.watchedshows', compact('watched_tvshow', 'covers_directory'));
	}

	public function showSettings()
	{
		return View::make('users.settings');
	}

	public function putPassword()
	{
		$current_password = Input::get('current_password');
		$new_password = Input::get('new_password');

		$update_user = User::find(Auth::id());
		if(!empty($current_password))
		{
			if(Hash::check($current_password, Auth::user()->password)) 
			{
				if(!empty($new_password))
					$update_user->password = Hash::make($new_password);
				else 
					return Redirect::back()->with('error', 'You cannot leave any fields empty!');
			}
			else
				return Redirect::back()->with('error', 'Incorrect current password was entered!');
			$update_user->save();
			return Redirect::back()->with('success', "Your password has been updated!");
		}
		else
			return Redirect::back()->with('error', 'You cannot leave any fields empty!');
		
	}

	public function putEmailAddress()
	{
		$email = Input::get('email');
		$validator = Validator::make(['email' => $email], ['email' => 'required|email']);
		if($validator->passes())
		{
			if($email == Auth::user()->email)
				return Redirect::back()->with('success', 'No changes were made to your email address!');
			else
			{
				$update_user = User::find(Auth::id());
				$update_user->email = $email;
				$update_user->save();
				return Redirect::back()->with('success', 'Your email address has been updated!');
			}
		}
		else
			return Redirect::back()->with('error', 'The entered email address was not a proper email address!');

	}

	public function putName()
	{

		Validator::extend('alpha_spaces', function($attribute, $value)
		{
			return preg_match('/^[\pL\s]+$/u', $value);
		});

		$name = Input::get('name');

		$validator = Validator::make(['name' => $name], ['name' => 'required|alpha_spaces|min:2|max:44']);
		if($validator->passes())
		{
			if($name == Auth::user()->name)
				return Redirect::back()->with('success', 'No changes were made to your name!');
			else
			{
				$update_user = User::find(Auth::id());
				$update_user->name = $name;
				$update_user->save();
				return Redirect::back()->with('success', 'Your name has been updated!');
			}
		}
		else
			return Redirect::back()->with('error', "Your name can't be empty and it can only have alphabets and should've a minimum length of 2!");

	}

	public function showProfile($username)
	{
		$user_profile = User::where('username','=', $username)->with('shows')->First();
		if(!empty($user_profile))
		{
			$uid = $user_profile->id;
			
			$watched_tvshows = TVShow::whereHas('watched_episode', function($query)
			use ($uid) {
				$query->where('uid','=', $uid)->where('tvid','>', 0);
			})->get();

			$watched_episodes = WatchedEpisode::where('uid', $uid)->with(['episode_data', 'tvshow'])->latest('id')->take(10)->get();

			$total_watched_shows = $watched_tvshows->count();
			$total_watched_episodes = WatchedEpisode::where('uid','=', $uid)->count();

			return View::make('users.profile', compact('user_profile', 'watched_tvshows', 'watched_episodes', 'total_watched_shows', 'total_watched_episodes'));
		}
		else
			App::abort(404, 'User not found');
	}

}