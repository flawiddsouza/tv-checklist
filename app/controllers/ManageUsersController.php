<?php

class ManageUsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$roles = Role::all();
		$users = User::orderBy('id','DESC')->get();

		if(Auth::user()->timezone != NULL)
			$timezone = Auth::user()->timezone;
		else
			$timezone = 'UTC';

		return View::make('admin.manage-users.index', compact('users','roles','timezone'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$roles = Role::lists('name', 'id');
		return View::make('admin.manage-users.add-user', compact('roles'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$username = Input::get('username');
		$password = Input::get('password');
		$email = Input::get('email');
		$role = Input::get('role');
		if(!empty($username) && !empty($password) && !empty($email) && !empty($role))
		{
			User::create([
							'username' => $username, 
							'password' => Hash::make($password), 
							'email' => $email, 
							'role' => $role
						]);
			return Redirect::action('ManageUsersController@index')->withSuccess('The new user has been successfully added!');
		}
		else
			return Redirect::back()->withError("Please submit a fully filled form!");
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		$roles = Role::all();
		return View::make('admin.manage-users.edit-user', compact('user', 'roles'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$username = Input::get('username');
		$password = Input::get('password');
		$email = Input::get('email');
		$role = Input::get('role');

		$update_user = User::find($id);
		if(!empty($username))
			$update_user->username = $username; 
		if(!empty($password))
			$update_user->password = Hash::make($password);
		if(!empty($email))
			$update_user->email = $email;
		if(!empty($role))
			$update_user->role = $role;
		$update_user->save();

		return Redirect::action('ManageUsersController@index')->withSuccess("The selected user has been successfully updated!");
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		// Deleting all the user data from these tables
		TVShowRating::where('uid','=', $id)->delete();
		WatchedTVShow::where('uid','=', $id)->delete();
		EpisodeRating::where('uid','=', $id)->delete();
		WatchedEpisode::where('uid','=', $id)->delete();

		// finally delete the user itself
		User::find($id)->delete();

		return Redirect::back()->withSuccess('The selected user and all related data has been deleted!');
	}


}
