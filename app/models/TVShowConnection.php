<?php

class TVShowConnection extends Eloquent {

	protected $table = 'tvshow_connections';

	protected $fillable = array('tvid', 'imdb_id', 'tvdb_id', 'tvrage_id', 'last_retrieval_time');
	
}