@extends('admin.default-layout')

@section('extra-title','Add Role')

@section('section-content')
<h1>Add Role</h1>
<div id="form" class="clearfix">
	{{ Form::open(array('action' => 'RolesController@store')) }}
	{{ Form::label('role_name', 'Role Name:', array('class'=>'set-display-block')) }}
	{{ Form::text('role_name', null, array('placeholder'=>'Role Name')) }}
	<br/>
	{{ Form::button('Add Role', array('class'=>'button button-medium button-input-width', 'type'=>'submit')) }}
	{{ Form::close() }}
</div>
@stop