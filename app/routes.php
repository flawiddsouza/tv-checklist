<?php

// sets the index page to the browse page, atleast for now
Route::get('/', function()
{
	return Redirect::action('BrowseController@showBrowse');
});


// show the registration form
Route::get('register', ['as' => 'register', 'uses' => 'UserController@showRegister'])->before('guest');

// process the registration form
Route::post('register', 'UserController@doRegister')->before('csrf');

// show the login form
Route::get('login', 'UserController@showLogin')->before('guest');

// process the login form
Route::post('login', 'UserController@doLogin')->before('csrf');

// process the logout request
Route::get('logout', ['as' => 'logout', 'uses' => 'UserController@doLogout']);

// show the forgot password form
Route::get('password/recover', 'RemindersController@getRemind')->before('guest');

// process the forgot password form
Route::post('password/recover', 'RemindersController@postRemind')->before('csrf');

// show the reset password form
Route::get('password/reset/{token}', 'RemindersController@getReset')->before('guest');

// process the reset password form
Route::post('password/reset/{token}', 'RemindersController@postReset')->before('csrf');

// route to My Shows
Route::get('myshows', 'UserController@showMyShows')->before('auth');

// process remove show request from the My Shows page
Route::post('myshows', 'UserController@doMyShows')->before(['auth|csrf']);

// logic for Watched Shows, there's no processing to be done here, this page only displays things
Route::get('watchedshows', 'UserController@WatchedShows')->before('auth');

// show the Settings page
Route::get('settings', 'UserController@showSettings')->before('auth');

// process the Settings page
// to change password
Route::patch('settings/password', 'UserController@putPassword')->before(['auth|csrf']);
// to change email address
Route::patch('settings/email', 'UserController@putEmailAddress')->before(['auth|csrf']);
// to change name
Route::put('settings/name', 'UserController@putName')->before(['auth|csrf']);

// resourceful route to requests controller
Route::resource('requests', 'RequestsController');
Route::patch('requests/{id}/close', 'RequestsController@close');
Route::patch('requests/{id}/reopen', 'RequestsController@reopen');

// show the profile page for the selected user
Route::get('profile/{username}', 'UserController@showProfile');


Route::group(array('prefix' => 'admin', 'before' => ['auth|admin']), function()
{
	// admin panel
	Route::get('/', 'AdminController@index');
	Route::post('/clearcache', 'AdminController@doClearCache');
	Route::post('/resetTVCacheCount', 'AdminController@doResetTVCacheCount');

	// add tv shows page
	Route::get('addshows', 'AdminController@AddShows');
	Route::post('addshows', 'AdminController@doAddShows');

	// manage tv shows
	Route::resource('manageshows', 'ManageTVShowsController');

	// manage episodes
	Route::resource('manageepisodes', 'ManageEpisodesController');
	Route::post('manageepisodes/totalseasons', 'ManageEpisodesController@getTotalSeasons');
	Route::post('manageepisodes/episodes', 'ManageEpisodesController@getEpisodeListing');

	//manage roles
	Route::resource('manageroles', 'RolesController');

	//manage users
	Route::resource('manageusers', 'ManageUsersController');
});



// logic for the browse page
Route::get('browse', 'BrowseController@showBrowse');

// process add request from the browse page
Route::post('browse', 'BrowseController@doBrowse')->before(['auth|csrf']);

// redirects to the browse page
Route::get('browse/series', function()
{
	return Redirect::action('BrowseController@showBrowse');
});

// logic for individual tv show pages (showname slug is just for url readabilty, it's not used to perform any functions)
Route::get('browse/series/{showid}/{showname}', 'BrowseController@individual_tvshow');
Route::get('browse/series/{showid}', 'BrowseController@individual_tvshow'); //so that the tvshow page can be accessed using the tvshow id, and without the tvshow name

// process add show requests from individual tv show pages
Route::post('browse/series/{showid}', 'BrowseController@add_watchedshow')->before(['auth|csrf']);

// process remove show requests from individual tv show pages
Route::delete('browse/series/{showid}', 'BrowseController@remove_watchedshow')->before(['auth|csrf']);

// process add rating to tv show from individual tv show pages 
//add_rating is basically just a dummy route URI to send the post method to this controller method, I don't know any other way
Route::post('browse/series/{showid}/add_rating', 'BrowseController@add_show_rating')->before('auth');

// process add watched episode/episodes from individual tv show pages 
Route::post('browse/series/{showid}/{season}/add_watched_episode', 'BrowseController@add_watched_episode')->before('auth');

// get episode description
Route::get('browse/series/{tvid}/{season}/{epnum}', 'BrowseController@getEpisodeDescription');


//search bar
Route::get('search', 'SearchController@index');

// embedding profiles
Route::get('profile/{username}/embed', function($username)
{
	$user_profile = User::where('username','=', $username)->First();
	if(!empty($user_profile))
		{
			$uid = $user_profile->id;
			
			$total_watched_shows = TVShow::whereHas('watched_episode', function($query)
			use ($uid) {
				$query->where('uid','=', $uid)->where('tvid','>', 0);
			})->count();

			$total_watched_episodes = WatchedEpisode::where('uid','=', $uid)->count();

			return View::make('embed', compact('user_profile', 'total_watched_shows', 'total_watched_episodes'));
		}
		else
			App::abort(404, 'User not found');
});