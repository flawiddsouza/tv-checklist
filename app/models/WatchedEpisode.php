<?php

class WatchedEpisode extends Eloquent {

	protected $table = 'watched_episode';

	protected $fillable = array('uid', 'tvid', 'season', 'episode', 'episode_id');

	public function episode_data()
	{
		return $this->belongsTo('Episode', 'episode_id');
	}

	public function tvshow()
	{
		return $this->belongsTo('TVShow', 'tvid');
	}

}