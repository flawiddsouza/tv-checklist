<?php

class Requests extends Eloquent {

	protected $table = 'requests';

	protected $fillable = ['uid', 'tvshow_name', 'first_aired_year', 'imdb_id', 'status', 'completed','completed_on'];

	protected $dates = ['completed_on'];

}